package edu.gzhu.guli.front.service.impl;

import com.aliyuncs.exceptions.ClientException;
import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.core.entity.Video;
import edu.gzhu.guli.core.repository.VideoRepository;
import edu.gzhu.guli.front.dto.VideoInfoDto;

import edu.gzhu.guli.front.service.VideoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class VideoServiceImpl implements VideoService {

    @Autowired
    private VideoRepository videoRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ApplicationResult<VideoInfoDto> getVideoById(Long videoId, VideoInfoDto videoInfoDto) throws ClientException {
        Optional<Video> optionalVideo = videoRepository.findById(videoId);
        if (!optionalVideo.isPresent())
            return ApplicationResult.failure("未获取到id为："+ videoId + "的视频");
        Video video = optionalVideo.get();
        modelMapper.map(video, videoInfoDto);
        return ApplicationResult.succese(videoInfoDto);
    }

    @Override
    public ApplicationResult<String> getVideoSourceById(String id){
        Optional<Video> optionalVideo = videoRepository.findById(Long.parseLong(id));
        if (!optionalVideo.isPresent())
            return ApplicationResult.failure("未获取到id为："+ id + "的视频");
        Video video = optionalVideo.get();
        return ApplicationResult.succese(video.getSourceId());
    }
}
