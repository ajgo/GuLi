package edu.gzhu.guli.front.controller;

import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.front.service.OrderService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@CrossOrigin
@RequestMapping("/order")
public class OrderController {

    @Resource
    private OrderService orderService;

    @RequestMapping("buy")
    public ApplicationResult<String> buyCourse(String username, String courseId){
        return orderService.buyCourse(username,courseId);
    }
}
