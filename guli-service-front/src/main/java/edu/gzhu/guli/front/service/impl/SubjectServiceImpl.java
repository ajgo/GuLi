package edu.gzhu.guli.front.service.impl;

import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.core.entity.Course;
import edu.gzhu.guli.core.entity.Subject;
import edu.gzhu.guli.core.repository.SubjectRepository;
import edu.gzhu.guli.front.dto.CourseSimpleDto;
import edu.gzhu.guli.front.dto.SubjectInfoDto;
import edu.gzhu.guli.front.service.SubjectService;
import edu.gzhu.guli.front.dto.SubjectDto;
import io.swagger.v3.oas.annotations.Operation;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubjectServiceImpl implements SubjectService {

    @Resource
    private SubjectRepository subjectRepository;

    @Resource
    private ModelMapper modelMapper;

    // 获取所有课程分类(树状)
    @Override
    public ApplicationResult<List<SubjectDto>> getAllSubject() {
        try {
            // 查询level为1的第1层分类
            List<SubjectDto> first = subjectRepository.findByLevelOrderBySortAsc(1)
                    .stream()
                    .map(it -> modelMapper.map(it, SubjectDto.class))
                    .collect(Collectors.toList());

            // 分别查询对应第二层分类，构造树型结构
            for (int i = 0; i < first.toArray().length; i++) {

                List<SubjectDto> second = subjectRepository.findByLevelAndPidOrderBySortAsc(2, first.get(i).getId())
                        .stream()
                        .map(it -> modelMapper.map(it, SubjectDto.class))
                        .collect(Collectors.toList());

                first.get(i).setChildren(second);
            }

            return ApplicationResult.succese(first);

        } catch (Exception e) {
            e.printStackTrace();
            return ApplicationResult.failure("获取失败");
        }
    }

    @Operation(summary = "获取主题信息", description = "用于轮播图左侧")
    @Override
    public ApplicationResult<List<SubjectInfoDto>> getAllSubjectSimpleInfo(int level) {
        List<Subject> subjectList = subjectRepository.findByLevelOrderBySortAsc(level);
        if (subjectList == null || subjectList.isEmpty())
            return ApplicationResult.failure("指定主题: " + level + "目标信息不存在");
        List<SubjectInfoDto> subjectInfoDtoList = subjectList
                .stream()
                .map(subject -> modelMapper.map(subject, SubjectInfoDto.class))
                .collect(Collectors.toList());
        for (int i = 0; i < subjectList.size(); i++) {
            List<Course> allCourses = new ArrayList<>();
            if (level <= 1) {
                allCourses = subjectList.get(i).getAllCourses();
            } else {
                allCourses = subjectList.get(i).getCourses();
            }
            List<CourseSimpleDto> courseSimpleDtoList = allCourses
                    .stream()
                    .map(course -> modelMapper.map(course, CourseSimpleDto.class))
                    .collect(Collectors.toList());
            subjectInfoDtoList.get(i).setCourseSimpleDtoList(courseSimpleDtoList);
        }
        return ApplicationResult.succese(subjectInfoDtoList);
    }

}
