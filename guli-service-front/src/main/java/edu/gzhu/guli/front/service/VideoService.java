package edu.gzhu.guli.front.service;

import com.aliyuncs.exceptions.ClientException;
import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.front.dto.VideoInfoDto;

public interface VideoService {

    ApplicationResult<VideoInfoDto> getVideoById(Long videoId, VideoInfoDto videoInfoDto) throws ClientException;

    ApplicationResult<String> getVideoSourceById(String id);
}
