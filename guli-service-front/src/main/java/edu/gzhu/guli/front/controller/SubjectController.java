package edu.gzhu.guli.front.controller;

import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.front.dto.SubjectDto;
import edu.gzhu.guli.front.dto.SubjectInfoDto;
import edu.gzhu.guli.front.service.SubjectService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/subject")
public class SubjectController {

    @Autowired
    private SubjectService subjectService;

    // 获取课程分类信息
    @GetMapping(value = "getAllSubject")
    public ApplicationResult<List<SubjectDto>> getAllSubject() {
        return subjectService.getAllSubject();

    }

    @GetMapping("getAllSubjectSimpleInfo/{level}")
    public  ApplicationResult<List<SubjectInfoDto>> getAllSubjectSimpleInfo(@PathVariable(name = "level",required = false) Integer level){
        ApplicationResult<List<SubjectInfoDto>> allSubjectSimpleInfo = subjectService.getAllSubjectSimpleInfo(level);
        return allSubjectSimpleInfo;
    }

}
