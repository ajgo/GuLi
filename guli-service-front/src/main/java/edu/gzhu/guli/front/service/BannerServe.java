package edu.gzhu.guli.front.service;

import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.front.dto.BannerDto;

import java.util.List;

public interface BannerServe {

    ApplicationResult<List<BannerDto>> getAllBanner();
}
