package edu.gzhu.guli.front.dto;


import lombok.Data;


import java.io.Serializable;

@Data
public class BannerDto implements Serializable {

    private static final long serialVersionUID = 1L;

    // <a>标签的title属性
    private String title;

    // <img>标签的src属性
    private String imageUrl;

    // <a>标签的href属性
    private String linkUrl;

    // 排序规则
    private int sort = 0;
}
