package edu.gzhu.guli.front.controller;


import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.front.dto.BannerDto;
import edu.gzhu.guli.front.dto.CourseInfoDto;
import edu.gzhu.guli.front.dto.CourseListItemDto;
import edu.gzhu.guli.front.dto.SubjectInfoDto;
import edu.gzhu.guli.front.service.BannerServe;
import edu.gzhu.guli.front.service.CourseService;
import edu.gzhu.guli.front.service.SubjectService;
import edu.gzhu.guli.front.service.VideoService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Tag(name = "课程信息查询接口",description = "可以对课程内容进行查询")
@RestController
@RequestMapping("/course")
@CrossOrigin
public class CourseController {

    @Autowired
    private CourseService courseService;

    @Autowired
    private BannerServe bannerServe;

    @Autowired
    private VideoService videoService;

    @Autowired
    private SubjectService subjectService;
    /**
     * 课程检索
     * @param searchContent 检索字段(xxABxx模糊搜索)
     * @param topSubjectId 顶级课程分类id
     * @param subjectId 次级课程分类id
     * @param listType 搜索方式，即排序方式["最热","最新","价格最高","价格最低"](最热为播放量降序排序，最新为create_time最新)
     * @param page 页码(1开始)
     * @param limit 每页查询数
     * @return
     */
    @GetMapping(value = "getCourses")
    public ApplicationResult<List<CourseListItemDto>> getCourses(String searchContent,String topSubjectId,String subjectId,
                                                                 String listType,int page,int limit) {
        return courseService.getCourses(searchContent,topSubjectId,subjectId,listType,page,limit);
    }

    @GetMapping("getCourseDetails/{courseId}")
    public ApplicationResult<CourseInfoDto> getCourseDetails(@PathVariable(name = "courseId") Long courseId, CourseInfoDto courseInfoDto){
        ApplicationResult<CourseInfoDto> result = courseService.getCourseDetailsById(courseId, courseInfoDto);
        return result;
    }

    @GetMapping(value = "getBanners")
    public ApplicationResult<List<BannerDto>> getBanners(){
        ApplicationResult<List<BannerDto>> result = bannerServe.getAllBanner();
        return result;
    }

    @GetMapping(value = "getHomePageInfo")
    public ApplicationResult<HashMap<String, Object>> getHomePageInfo() {
        ApplicationResult<List<BannerDto>> allBanner = bannerServe.getAllBanner();
        ApplicationResult<List<CourseListItemDto>> hotCourseList = courseService.getCourses("", "", "", "最热", 1, 10);
        ApplicationResult<List<CourseListItemDto>> newCourseList = courseService.getCourses("", "", "", "最新", 1, 15);
        ApplicationResult<List<SubjectInfoDto>> allSubjectSimpleInfo = subjectService.getAllSubjectSimpleInfo(2);
        List<List<CourseListItemDto>> courseList = new ArrayList<>();
        for (SubjectInfoDto subjectInfoDto : allSubjectSimpleInfo.getData()) {
            ApplicationResult<List<CourseListItemDto>> courses = courseService.getCourses("", String.valueOf(subjectInfoDto.getId()), "", "", 1, 10);
            courseList.add(courses.getData());
        }
        HashMap<String, Object> result = new HashMap<>();
        result.put("allBanner", allBanner.getData());
        result.put("hotCourseList", hotCourseList.getData());
        result.put("newCourseList", newCourseList.getData());
        result.put("courseList", courseList);
        result.put("allSubject", allSubjectSimpleInfo.getData());
        return ApplicationResult.succese(result);
    }

    @RequestMapping("/video/auth")
    public ApplicationResult<String> getVideo(String id){
        return courseService.getVideoAuthById(id);
    }

    @RequestMapping("/video/source")
    public ApplicationResult<String> getSource(String id){
        return videoService.getVideoSourceById(id);
    }
}
