package edu.gzhu.guli.front.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
@Schema
public class MemberInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Schema(name = "用户名")
    private String username;

    @Schema(name = "真实姓名")
    private String name;

    @Schema(name = "微信openid")
    private String openid;

    @Schema(name = "手机号")
    private String phone;

    @Schema(name = "密码")
    private String password;

    @Schema(name = "性别 1 女，2 男")
    private Integer sex;

    @Schema(name = "年龄")
    private Integer age;

    @Schema(name = "用户头像")
    private String avatar;

    @Schema(name = "用户签名")
    private String sign;

    @Schema(name = "用户角色")
    private String role;

    @Schema(name = "加入的课程")
    private List<CourseInfoDto> courseInfoDtoList;

    @Schema(name = "课程订单")
    private List<CourseOrderInfoDto> courseOrderInfoDtoList;
}
