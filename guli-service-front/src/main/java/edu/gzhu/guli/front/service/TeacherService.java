package edu.gzhu.guli.front.service;

import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.front.dto.TeacherDto;

import java.util.List;

public interface TeacherService {

    ApplicationResult<List<TeacherDto>> getAllTeachers();

    ApplicationResult<TeacherDto> getTeacher(String name);
}
