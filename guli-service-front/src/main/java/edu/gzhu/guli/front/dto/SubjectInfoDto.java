package edu.gzhu.guli.front.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Schema(name = "轮播图左侧主题信息")
@Data
public class SubjectInfoDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private long id;

    // 主题名称
    private String name;

    // 级别
    private int level;

    // 排序规则
    private int sort;

    // 主题下课程
    private List<CourseSimpleDto> courseSimpleDtoList;

}
