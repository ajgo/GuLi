package edu.gzhu.guli.front.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.gzhu.guli.core.entity.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class SubjectDto implements Serializable {

    /*
     * 父级主题ID
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;

    /*
     * 主题名称
     */
    private String name;

    /*
     * 级别
     */
    private int level;

    /*
     * 排序规则
     */
    private int sort;

    /*
     * 子分类
     */
    private List<SubjectDto> children;
}
