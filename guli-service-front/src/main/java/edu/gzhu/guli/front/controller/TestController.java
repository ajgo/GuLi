package edu.gzhu.guli.front.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
public class TestController {

  @GetMapping(value = "hello")
  public String test(@RequestParam String param) {
    return "yes";
  }

}
