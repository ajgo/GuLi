package edu.gzhu.guli.front.controller;

import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.front.dto.TeacherDto;
import edu.gzhu.guli.front.service.TeacherService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/teacher")
@CrossOrigin
public class TeacherController {

    @Resource
    private TeacherService teacherService;

    // 获取所有老师
    @RequestMapping("getAllTeachers")
    public ApplicationResult<List<TeacherDto>> getAllTeachers(){
        return teacherService.getAllTeachers();
    }

    // 根据姓名获取老师信息
    @RequestMapping("getTeacher")
    public ApplicationResult<TeacherDto> getTeacher(String name){
        return teacherService.getTeacher(name);
    }
}
