package edu.gzhu.guli.front.map;

import edu.gzhu.guli.core.entity.Course;
import edu.gzhu.guli.front.dto.CourseInfoDto;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class CourseDtoMap extends PropertyMap<Course, CourseInfoDto> {

    @Override
    protected void configure() {
        map().setSubjectLevelOneId(source.getTopSubject().getLevel());
        map().setSubjectLevelOne(source.getTopSubject().getName());
        map().setSubjectLevelTwoId(source.getSubject().getLevel());
        map().setSubjectLevelTwo(source.getSubject().getName());
    }
}
