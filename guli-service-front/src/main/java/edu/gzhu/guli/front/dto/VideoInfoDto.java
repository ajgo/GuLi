package edu.gzhu.guli.front.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

@Data
@Tag(name = "章节视频基本信息封装")
public class VideoInfoDto {

    // 视频标题
    private String title;

    // 点播id
    private String sourceId;

    // 播放次数
    private int playCount;

    // 视频时长
    private int duration;

    // 所属章节id
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private long chapterId;
}
