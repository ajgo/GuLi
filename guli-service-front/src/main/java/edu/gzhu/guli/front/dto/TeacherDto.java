package edu.gzhu.guli.front.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.gzhu.guli.core.entity.Course;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class TeacherDto implements Serializable {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private long id;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;

    private String name;

    private String introduce;

    private String career;

    private String avatar;

    private int level;

    private int sort;

    private List<CourseInfoDto> courses;
}
