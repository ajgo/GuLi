package edu.gzhu.guli.front.service;

import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.front.dto.SubjectDto;
import edu.gzhu.guli.front.dto.SubjectInfoDto;

import java.util.List;

public interface SubjectService {

    // 获取课程分类信息
    ApplicationResult<List<SubjectDto>> getAllSubject();

    // 获取轮播图左侧主题分类信息
    ApplicationResult<List<SubjectInfoDto>> getAllSubjectSimpleInfo(int level);
}
