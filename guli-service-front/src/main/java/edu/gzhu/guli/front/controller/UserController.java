package edu.gzhu.guli.front.controller;


import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.core.oss.OSSService;
import edu.gzhu.guli.front.dto.CommentInfoDto;
import edu.gzhu.guli.front.dto.CourseOrderInfoDto;
import edu.gzhu.guli.front.dto.MemberInfoDto;
import edu.gzhu.guli.front.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Tag(name = "用户接口", description = "用户个人信息相关功能")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private OSSService ossService;

    @GetMapping("getMemberInfo/{memberId}")
    public ApplicationResult<MemberInfoDto> getMemberInfo(@PathVariable(name = "memberId") Long memberId, MemberInfoDto memberInfoDto) {
        ApplicationResult<MemberInfoDto> result = userService.getMemberInfoById(memberId, memberInfoDto);
        return result;
    }

    @GetMapping("uploadImage")
    public ApplicationResult<String> uploadImage(MultipartFile file) {
        String fileName = ossService.uploadFile(file, "image");
        String url = ossService.getSingeNatureUrl(fileName);
        return ApplicationResult.succese(url);
    }

    @PostMapping("userComment")
    public ApplicationResult<CommentInfoDto> userComment(CommentInfoDto commentInfoDto){
        ApplicationResult<CommentInfoDto> result = userService.userComment(commentInfoDto);
        return result;
    }

    @PostMapping("userRegister")
    public ApplicationResult<MemberInfoDto> userRegister(MemberInfoDto memberInfoDto){
        ApplicationResult<MemberInfoDto> result = userService.userRegister(memberInfoDto);
        return result;
    }

    @PutMapping("userUpdate")
    public ApplicationResult<MemberInfoDto> userUpdate(@RequestBody MemberInfoDto memberInfoDto){
        ApplicationResult<MemberInfoDto> result = userService.userUpdate(memberInfoDto);
        return result;
    }
}
