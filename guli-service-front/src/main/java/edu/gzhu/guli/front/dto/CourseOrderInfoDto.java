package edu.gzhu.guli.front.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Schema(name = "课程订单信息",description = "用户信息下的个人订单信息")
public class CourseOrderInfoDto implements Serializable {

    private static final long serialVersionUID = 1L;

    // 商品名称
    private String title;

    private String number;

    private float totalFee;

    private LocalDateTime createTime;


}
