package edu.gzhu.guli.front.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Tag(name = "课程详情封装对象",description = "封装课程详情信息")
public class CourseInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;

    @Schema(name = "课程标题")
    private String title;

    @Schema(name = "课程销售价格，设置为0则可免费观看")
    private float price;

    @Schema(name = "课程简介")
    private String description;

    @Schema(name = "课程数量")
    private Integer lessonNum;

    @Schema(name = "课程封面图片路径")
    private String cover;

    @Schema(name = "销售数量")
    private Long buyCount;

    @Schema(name = "观看数量")
    private Long viewCount;

    @Schema(name = "课程状态")
    private int status;

    @Schema(name = "讲师ID")
    private String teacherId;

    @Schema(name = "讲师姓名")
    private String teacherName;

    @Schema(name = "讲师资历,一句话说明讲师")
    private String teacherIntroduce;

    @Schema(name = "讲师头像")
    private String teacherAvatar;

    @Schema(name = "课程类别ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private long subjectLevelOneId;

    @Schema(name = "类别名称")
    private String subjectLevelOne;

    @Schema(name = "课程类别ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private long subjectLevelTwoId;

    @Schema(name = "类别名称")
    private String subjectLevelTwo;

    @Schema(name = "用户评论")
    private List<CommentInfoDto> commentInfoDtoList;

    @Schema(name = "课程章节")
    private List<ChapterInfoDto> chapterInfoDtoList;
}
