package edu.gzhu.guli.front.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentInfoDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private long id;

    // 用户名
    private String name;

    // 评论用户的头像url
    private String avatar;

    // 评论的课程id
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private long courseId;

    // 用户id
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private long memberId;

    // 评论的内容
    private String content;

    // 给课程的评分
    private int score = 5;

}
