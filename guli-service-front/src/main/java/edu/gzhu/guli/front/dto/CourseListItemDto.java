package edu.gzhu.guli.front.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.gzhu.guli.core.entity.BaseEntity;
import edu.gzhu.guli.core.entity.Subject;
import edu.gzhu.guli.core.entity.Teacher;
import lombok.Data;
import org.springframework.data.annotation.LastModifiedDate;

import java.io.Serializable;
import java.time.LocalDateTime;

/*
 * 课程列表，包括一些课程的简要信息
 */
@Data
public class CourseListItemDto implements Serializable {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;

    /*
     * 课程名
     */
    private String title;

    /*
     * 课程描述 （可以考虑markdown格式）
     */
    private String description;

    /*
     * 课程价格
     */
    private float price = 0.0f;

    /*
     * 课程数量
     */
    private int lessonNum;

    /*
     * 课程封面图片url
     */
    private String cover;

    /*
     * 课程的购买数量
     */
    private int buyCount;

    /*
     * 课程的观看数量
     */
    private int viewCount;

    /*
     * 课程状态（0代表为草稿，1代表为已发布）
     */
    private int status;
}
