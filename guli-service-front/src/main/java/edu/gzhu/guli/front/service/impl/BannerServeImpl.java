package edu.gzhu.guli.front.service.impl;

import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.core.entity.Banner;
import edu.gzhu.guli.core.repository.BannerRepository;
import edu.gzhu.guli.front.dto.BannerDto;
import edu.gzhu.guli.front.service.BannerServe;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BannerServeImpl implements BannerServe {

    @Autowired
    private BannerRepository bannerRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ApplicationResult<List<BannerDto>> getAllBanner() {
        List<Banner> bannerList = bannerRepository.findByOrderBySortAsc();
        if (bannerList != null && !bannerList.isEmpty()){
            List<BannerDto> bannerDtoList = bannerList
                    .stream()
                    .map(banner -> modelMapper.map(banner, BannerDto.class))
                    .collect(Collectors.toList());
            return ApplicationResult.succese(bannerDtoList);
        }
        return ApplicationResult.failure("未找到轮播图信息");
    }
}
