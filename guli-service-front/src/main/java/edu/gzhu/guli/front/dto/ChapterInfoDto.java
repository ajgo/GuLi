package edu.gzhu.guli.front.dto;


import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;


@Data
@Tag(name = "课程章节基本信息封装",description = "封装课程下的章节信息")
public class ChapterInfoDto {

    // id
    private String id;

    // 标题
    private String title;


    // 排序规则
    private int sort;

    // 是否可以预览
    private boolean canReview = false;

    // 章节视频
    private VideoInfoDto videoInfoDto;
}
