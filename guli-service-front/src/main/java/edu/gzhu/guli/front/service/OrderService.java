package edu.gzhu.guli.front.service;

import edu.gzhu.guli.core.common.ApplicationResult;

public interface OrderService {

    ApplicationResult<String> buyCourse(String username, String courseId);
}
