package edu.gzhu.guli.front.service;

import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.front.dto.CommentInfoDto;
import edu.gzhu.guli.front.dto.MemberInfoDto;

public interface UserService {
    ApplicationResult<MemberInfoDto> getMemberInfoById(Long id, MemberInfoDto memberInfoDto);

    ApplicationResult<CommentInfoDto> userComment(CommentInfoDto commentInfoDto);

    ApplicationResult<MemberInfoDto> userRegister(MemberInfoDto memberInfoDto);

    ApplicationResult<MemberInfoDto> userUpdate(MemberInfoDto memberInfoDto);
}

