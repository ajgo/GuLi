package edu.gzhu.guli.front.service.impl;

import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.core.repository.TeacherRepository;
import edu.gzhu.guli.front.dto.SubjectDto;
import edu.gzhu.guli.front.dto.TeacherDto;
import edu.gzhu.guli.front.service.TeacherService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherServiceImpl implements TeacherService {

    @Resource
    private ModelMapper modelMapper;

    @Resource
    private TeacherRepository teacherRepository;

    @Override
    public ApplicationResult<List<TeacherDto>> getAllTeachers(){
        try{
            List<TeacherDto> teachers = teacherRepository.findAll()
                    .stream()
                    .map(it -> modelMapper.map(it, TeacherDto.class))
                    .collect(Collectors.toList());

            return ApplicationResult.succese(teachers);

        }catch (Exception e){
            e.printStackTrace();
            return ApplicationResult.failure("获取失败");
        }
    }

    @Override
    public ApplicationResult<TeacherDto> getTeacher(String name){
        try{
            TeacherDto teacher = modelMapper.map(teacherRepository.findAllByName(name),TeacherDto.class);

            return ApplicationResult.succese(teacher);

        }catch (Exception e){
            e.printStackTrace();
            return ApplicationResult.failure("获取失败");
        }
    }
}
