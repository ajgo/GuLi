package edu.gzhu.guli.front.service;

import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.front.dto.CourseInfoDto;
import edu.gzhu.guli.front.dto.CourseListItemDto;
import edu.gzhu.guli.front.dto.SubjectInfoDto;
import io.swagger.v3.oas.annotations.Operation;

import java.util.List;

public interface CourseService {

    // 获取课程
    @Operation(summary = "获取课程")
    ApplicationResult<List<CourseListItemDto>> getCourses(String searchContent,String topSubjectId,String subjectId,
                                                          String listType,int page,int limit);
    @Operation(summary = "根据ID获取课程详情信息")
    ApplicationResult<CourseInfoDto> getCourseDetailsById(Long courseId, CourseInfoDto courseInfoDto);

    ApplicationResult<String> getVideoAuthById(String id);
}
