package edu.gzhu.guli.front;

import edu.gzhu.guli.front.service.CourseService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import edu.gzhu.guli.core.repository.RoleRepository;

@SpringBootTest
class GuLiServiceFrontTests {

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	CourseService courseService;

	@Test
	void contextLoads() {
	}
}
