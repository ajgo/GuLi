package edu.gzhu.guli.back.service.teacher;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import edu.gzhu.guli.back.service.course.CourseReadDto;
import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.core.entity.Teacher;
import edu.gzhu.guli.core.repository.TeacherRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class TeacherService {
  final TeacherRepository teacherRepository;
  final ModelMapper modelMapper;

  public ApplicationResult<List<TeacherNameReadDto>> loadAllTeacherName() {
     List<TeacherNameReadDto> data = teacherRepository.findAll()
      .stream().map(it -> modelMapper.map(it, TeacherNameReadDto.class))
      .collect(Collectors.toList());
    return ApplicationResult.succese(data);
  }

  public ApplicationResult<List<CourseReadDto>> loadCourseOfTeacher(Long teacherId) {
    Optional<Teacher> check = teacherRepository.findById(teacherId);
    if (!check.isPresent())
      return ApplicationResult.failure("未找到对应的讲师");
    Teacher teacher = check.get();
    List<CourseReadDto> data = teacher.getCourses().stream().map(it -> modelMapper.map(it, CourseReadDto.class))
        .collect(Collectors.toList());
    return ApplicationResult.succese(data);
  }

  public ApplicationResult<Page<TeacherReadDto>> loadAllPaged(int page, int size) {
    Page<Teacher> payload = teacherRepository.findAll(PageRequest.of(page, size));
    Page<TeacherReadDto> data = payload.map(it -> modelMapper.map(it, TeacherReadDto.class));
    return ApplicationResult.succese(data);
  }

  public ApplicationResult<TeacherReadDto> create(TeacherCreateOrEditDto dto) {
    Teacher teacher = modelMapper.map(dto, Teacher.class);
    teacher = teacherRepository.save(teacher);
    TeacherReadDto data = modelMapper.map(teacher, TeacherReadDto.class);
    return ApplicationResult.succese(data);
  }

  public ApplicationResult<TeacherReadDto> edit(Long teacherId, TeacherCreateOrEditDto dto) {
    Optional<Teacher> check = teacherRepository.findById(teacherId);
    if (!check.isPresent())
      return ApplicationResult.failure("未找到ID为：" + teacherId + "的讲师");

    Teacher teacher = check.get();
    modelMapper.map(dto, teacher);
    teacher = teacherRepository.save(teacher);
    TeacherReadDto data = modelMapper.map(teacher, TeacherReadDto.class);
    return ApplicationResult.succese(data);
  }

  public ApplicationResult<Object> delete(Long teacherId) {
    Optional<Teacher> check = teacherRepository.findById(teacherId);
    if (!check.isPresent())
      return ApplicationResult.failure("未找到ID为：" + teacherId + "的讲师");
    Teacher teacher = check.get();
    boolean hasCourses = teacher.getCourses().isEmpty();
    if (hasCourses)
      return ApplicationResult.failure("无法删除正在参与课程的讲师");

    teacherRepository.delete(teacher);
    return ApplicationResult.succese();
  }

}
