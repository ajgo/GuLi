package edu.gzhu.guli.back.service.subject;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class SubjectCreateOrEditDto {
  /*
   * 父级主题ID
   */
  private Long pid;

  /*
   * 主题名称
   */
  @NotNull
  @NotBlank
  private String name;

  /*
   * 级别
   */
  private int level;
  
  /*
  * 排序规则
  */
  private int sort;
}
