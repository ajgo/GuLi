package edu.gzhu.guli.back.service.course.video;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

import edu.gzhu.guli.core.entity.Video;
import lombok.Data;

@Data
public class VideoCreateOrEditDto {
  /*
   * 视频标题
   */
  @NotNull
  @NotBlank
  private String title;

  /*
   * 阿里云视频上传后返回的ID
   */
  @NotNull
  @NotBlank
  private String source;

  /*
   * 
   */
  @NotNull
  @NotBlank
  private String orginalName;

  /*
   * 视频时长
   */
  private int duration;
}

@Component
class VideoCreateOrEditDtoMapper extends PropertyMap<VideoCreateOrEditDto, Video> {

  @Override
  protected void configure() {
    map().setSourceId(source.getSource()); 
  }
  
}