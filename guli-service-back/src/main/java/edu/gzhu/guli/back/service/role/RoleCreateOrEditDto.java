package edu.gzhu.guli.back.service.role;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class RoleCreateOrEditDto {
  /*
   * 角色名称
   * 示例： ADMIN, ROOT, OTHER
   */
  @NotNull
  @NotBlank
  private String name;

  /*
   * 对角色的功能描述
   */
  private String description;
}
