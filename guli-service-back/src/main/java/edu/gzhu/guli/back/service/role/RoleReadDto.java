package edu.gzhu.guli.back.service.role;


import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class RoleReadDto {

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private Long id;

  /*
   * 角色名称
   * 示例： ADMIN, ROOT, OTHER
   */
  private String name;

  /*
   * 对角色的功能描述
   */
  private String description;

  private LocalDateTime createTime;

  private LocalDateTime modifyTime;
}
