package edu.gzhu.guli.back.service.admin;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class AdminReadDto {

  private Long id;

  /*
   * 用户名
   */
  private String username;

  /*
   * 姓名
   */
  private String name;

  /*
   * 拥有的角色名称
   */
  private String roleName;

  private LocalDateTime createTime;

  private LocalDateTime modifyTime;
}
