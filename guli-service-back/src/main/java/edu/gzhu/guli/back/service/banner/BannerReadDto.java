package edu.gzhu.guli.back.service.banner;

import lombok.Data;

@Data
public class BannerReadDto {

  private Long id;

  /*
   * <a>标签的title属性
   */
  private String title;
  
  /*
   * <img>标签的src属性
   */
  private String imageUrl;
  
  /*
   * <a>标签的hrel属性 
   */
  private String linkUrl;

  /*
   * 排序规则
   */
  private int sort = 0;
}
