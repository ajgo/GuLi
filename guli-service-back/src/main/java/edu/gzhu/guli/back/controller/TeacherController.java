package edu.gzhu.guli.back.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.gzhu.guli.back.service.course.CourseReadDto;
import edu.gzhu.guli.back.service.teacher.TeacherCreateOrEditDto;
import edu.gzhu.guli.back.service.teacher.TeacherNameReadDto;
import edu.gzhu.guli.back.service.teacher.TeacherReadDto;
import edu.gzhu.guli.back.service.teacher.TeacherService;
import edu.gzhu.guli.core.common.ApplicationResult;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("api/teacher")
@AllArgsConstructor
public class TeacherController {
  final TeacherService teacherService;
  final ModelMapper modelMapper;

  @GetMapping("{teacherId}/course")
  public ApplicationResult<List<CourseReadDto>> loadCourseOfTeacher(
    @PathVariable Long teacherId
  ) {
    return teacherService.loadCourseOfTeacher(teacherId);
  }

  @GetMapping
  public ApplicationResult<Page<TeacherReadDto>> loadAllPaged(
    @RequestParam(defaultValue = "0") int page,
    @RequestParam(defaultValue = "10") int size
  ) {
    return teacherService.loadAllPaged(page, size);
  }

  @GetMapping("name")
  public ApplicationResult<List<TeacherNameReadDto>> loadAllTeacherName() {
    return teacherService.loadAllTeacherName();
  }

  @PostMapping
  public ApplicationResult<TeacherReadDto> create(@Valid @RequestBody TeacherCreateOrEditDto dto) {
    return teacherService.create(dto);
  }

  @PutMapping("{teacherId}")
  public ApplicationResult<TeacherReadDto> edit(@PathVariable Long teacherId,
      @Valid @RequestBody TeacherCreateOrEditDto dto) {
    return teacherService.edit(teacherId, dto);
  }

  @DeleteMapping("{teacherId}")
  public ApplicationResult<Object> delete(@PathVariable Long teacherId) {
    return teacherService.delete(teacherId);
  }

}
