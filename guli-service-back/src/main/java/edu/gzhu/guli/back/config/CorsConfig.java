package edu.gzhu.guli.back.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfig implements WebMvcConfigurer {

  private static String[] exposedHeaders = new String[] {
      "access-control-allow-headers",
      "access-control-allow-methods",
      "access-control-allow-origin",
      "access-control-max-age",
      "Authorization",
  };

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**")
        .allowedOriginPatterns("*")
        .allowedHeaders("*")
        .exposedHeaders(exposedHeaders)
        .allowCredentials(true)
        .allowedMethods("*")
        .maxAge(3600);
  }
}