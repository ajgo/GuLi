package edu.gzhu.guli.back.service.banner;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class BannerCreateOrEditDto {

  /*
   * <a>标签的title属性
   */
  @NotNull
  @NotBlank
  private String title;

  /*
   * <img>标签的src属性
   */
  @NotNull
  @NotBlank
  private String imageUrl;

  /*
   * <a>标签的hrel属性
   */
  @NotNull
  @NotBlank
  private String linkUrl;

  /*
   * 排序规则
   */
  private int sort = 0;
}
