package edu.gzhu.guli.back.service.teacher;

import java.util.ArrayList;
import java.util.List;

import edu.gzhu.guli.back.service.course.CourseReadDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class TeacherReadWithCourseDto extends TeacherReadDto {
  List<CourseReadDto> courses = new ArrayList<>();
}
