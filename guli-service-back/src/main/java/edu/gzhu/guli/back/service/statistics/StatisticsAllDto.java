package edu.gzhu.guli.back.service.statistics;

import java.time.LocalDate;

import lombok.Data;

@Data
public class StatisticsAllDto {
  /*
   * 统计数据的日次
   */
  private LocalDate date;

  /*
   * 当天登录人数
   */
  private int loginNumber;

  /*
   * 当天注册人数
   */
  private int registerNumber;

  /*
   * 当天视频观看的数量
   */
  private int videoViewNumber;

  /*
   * 当天的课程数量
   */
  private int courseNumber;
}
