package edu.gzhu.guli.back.service.course.chapter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ChapterCreateOrEditDto {
  /*
   * 章节名称
   */
  @NotNull
  @NotBlank
  private String title;

  /*
   * 排序规则
   */
  private int sort = 0;

  /*
   * 是否可以预览
   */
  private boolean canReview = false;
}
