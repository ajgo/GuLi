package edu.gzhu.guli.back.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.gzhu.guli.back.service.role.RoleCreateOrEditDto;
import edu.gzhu.guli.back.service.role.RoleReadDto;
import edu.gzhu.guli.back.service.role.RoleService;
import edu.gzhu.guli.core.common.ApplicationResult;
import lombok.AllArgsConstructor;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping("api/role")
@AllArgsConstructor
public class RoleController {

  final RoleService roleService;

  @GetMapping
  public ApplicationResult<List<RoleReadDto>> loadAllRole() {
    return roleService.loadAllRole();
  }

  @PostMapping
  public ApplicationResult<RoleReadDto> create(@Valid @RequestBody RoleCreateOrEditDto dto) {
    return roleService.create(dto);
  }

  @PutMapping(value = "{roleId}")
  public ApplicationResult<RoleReadDto> edit(@PathVariable Long roleId, @Valid @RequestBody RoleCreateOrEditDto dto) {
    return roleService.edit(roleId, dto);
  }

  @DeleteMapping(value = "{roleId}")
  public ApplicationResult<Object> delete(@PathVariable Long roleId) {
    return roleService.delete(roleId);
  }

}
