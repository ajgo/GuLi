package edu.gzhu.guli.back.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.gzhu.guli.back.service.course.chapter.ChapterCreateOrEditDto;
import edu.gzhu.guli.back.service.course.chapter.ChapterReadDto;
import edu.gzhu.guli.back.service.course.chapter.ChapterService;
import edu.gzhu.guli.core.common.ApplicationResult;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("api/course")
public class ChapterController {
  final ChapterService chapterService;

  @PostMapping("{courseId}/chatper")
  public ApplicationResult<ChapterReadDto> create(
    @PathVariable Long courseId,
    @Valid @RequestBody ChapterCreateOrEditDto dto) {
    return chapterService.create(courseId, dto);
  }

  @PutMapping("{courseId}/chapter/{chapterId}")
  public ApplicationResult<ChapterReadDto> edit(
    @PathVariable Long courseId, @PathVariable Long chapterId,
    @Valid @RequestBody ChapterCreateOrEditDto dto
  ) {
    return chapterService.edit(courseId, chapterId, dto);
  }

  @DeleteMapping("{courseId}/chapter/{chapterId}")
  public ApplicationResult<Object> delete(
    @PathVariable Long courseId, @PathVariable Long chapterId
  ) {
    return chapterService.delete(chapterId);
  }

}
