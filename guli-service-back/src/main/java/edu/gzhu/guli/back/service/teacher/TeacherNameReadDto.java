package edu.gzhu.guli.back.service.teacher;


import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class TeacherNameReadDto {
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private Long id;
   /*
   * 姓名
   */
  private String name;
}
