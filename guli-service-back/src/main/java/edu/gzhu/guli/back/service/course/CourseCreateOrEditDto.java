package edu.gzhu.guli.back.service.course;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CourseCreateOrEditDto {
    /*
   * 课程名
   */
  @NotNull
  @NotBlank
  private String title;
  
  /*
  * 课程描述 （可以考虑markdown格式）
  */
  @NotNull
  @NotBlank
  private String description;
  
  /*
  * 课程价格
  */
  private float price = 0.0f;
  
  /*
  * 课程数量
  */
  private int lessonNum;
  
  /*
  * 课程封面图片url
  */
  @NotNull
  @NotBlank
  private String cover;
  
  /*
  * 课程状态（0代表为草稿，1代表为已发布）
  */
  private boolean status;

  /*
   * 授课教师ID
   * 可以为null
   */
  private Long teacherId;

  /*
   * 父级主题id
   * 可以为空
   */
  private Long topSubjectId;

  /*
   * 主题id
   * 可以为空
   */
  private Long subjectId;
}