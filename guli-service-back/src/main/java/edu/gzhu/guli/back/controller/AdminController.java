package edu.gzhu.guli.back.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.gzhu.guli.back.service.admin.AdminCreateOrEditDto;
import edu.gzhu.guli.back.service.admin.AdminReadDto;
import edu.gzhu.guli.back.service.admin.AdminService;
import edu.gzhu.guli.core.common.ApplicationResult;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("api/admin")
@AllArgsConstructor
public class AdminController {
  final AdminService adminService;

  @GetMapping
  public ApplicationResult<List<AdminReadDto>> loadAllAdmin() {
    return adminService.loadAllAdmin();
  }

  @PostMapping
  public ApplicationResult<AdminReadDto> create(@Valid @RequestBody AdminCreateOrEditDto dto) {
    return adminService.create(dto);
  }

  @PutMapping(value = "{adminId}")
  public ApplicationResult<AdminReadDto> edit(@PathVariable Long adminId,
      @Valid @RequestBody AdminCreateOrEditDto dto) {
    return adminService.edit(adminId, dto);
  }

  @DeleteMapping(value = "{adminId}")
  public ApplicationResult<Object> delete(@PathVariable Long adminId) {
    return adminService.delete(adminId);
  }
}
