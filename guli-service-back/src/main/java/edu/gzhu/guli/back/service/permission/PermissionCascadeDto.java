package edu.gzhu.guli.back.service.permission;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PermissionCascadeDto {

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private Long id;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private Long pid;

  private int code;

  private String name;

  private String url;

  private int level;

  private List<PermissionCascadeDto> children;

  public static PermissionCascadeDto of(Long id, Long pid, int code, String name, String url, int level) {
    if (level == 3) {
      return withoutChildren(id, pid, code, name, url, level);
    }
    return withChildren(id, pid, code, name, url, level);
  }

  private static PermissionCascadeDto withChildren(Long id, Long pid, int code, String name, String url, int level) {
    return new PermissionCascadeDto(id, pid, code, name, url, level, new ArrayList<>());
  }

  private static PermissionCascadeDto withoutChildren(Long id, Long pid, int code, String name, String url, int level) {
    return new PermissionCascadeDto(id, pid, code, name, url, level, null);
  }

  public void addChildren(PermissionCascadeDto cascadeDto) {
    children.add(cascadeDto);
  }
}
