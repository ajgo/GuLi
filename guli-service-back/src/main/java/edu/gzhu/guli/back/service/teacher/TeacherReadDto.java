package edu.gzhu.guli.back.service.teacher;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class TeacherReadDto {

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private Long id;
   /*
   * 姓名
   */
  private String name;
  
  /*
  * 介绍
  */
  private String introduce;
  
  /*
  * 职称
  */
  private String career;

  /*
   * 头像url
   */
  private String avatar;

  /*
   * 
   */
  private int level;

  /*
   * 排序方式
   */
  private int sort;

}
