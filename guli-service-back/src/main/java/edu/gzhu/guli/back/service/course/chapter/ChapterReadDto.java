package edu.gzhu.guli.back.service.course.chapter;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import edu.gzhu.guli.back.service.course.video.VideoReadDto;
import lombok.Data;

@Data
public class ChapterReadDto {

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private Long id;

  private String title;

  private int sort;

  private boolean canReview;

  private VideoReadDto video;
}
