package edu.gzhu.guli.back.service.teacher;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class TeacherCreateOrEditDto {
    /*
   * 姓名
   */
  @NotNull
  @NotBlank
  private String name;
  
  /*
  * 介绍
  */
  @NotNull
  @NotBlank
  private String introduce;
  
  /*
  * 职称
  */
  @NotNull
  @NotBlank
  private String career;

  /*
   * 头像url
   */
  @NotNull
  @NotBlank
  private String avatar;

  /*
   * 
   */
  private int level;

  /*
   * 排序方式
   */
  private int sort;
}
