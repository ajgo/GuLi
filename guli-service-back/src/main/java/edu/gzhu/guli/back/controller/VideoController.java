package edu.gzhu.guli.back.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.gzhu.guli.back.service.course.video.ChapterVideoService;
import edu.gzhu.guli.back.service.course.video.VideoCreateOrEditDto;
import edu.gzhu.guli.back.service.course.video.VideoReadDto;
import edu.gzhu.guli.core.common.ApplicationResult;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("api/chapter")
@AllArgsConstructor
public class VideoController {
  final ChapterVideoService chapterVideoService;

  @PostMapping("{chapterId}/video")
  public ApplicationResult<VideoReadDto> create(@PathVariable Long chapterId, @Valid @RequestBody VideoCreateOrEditDto dto) {
    return chapterVideoService.create(chapterId, dto);
  }

  @PutMapping("{chapterId}/video/{videoId}")
  public ApplicationResult<VideoReadDto> edit(
    @PathVariable Long chapterId, @PathVariable Long videoId,
    @Valid @RequestBody VideoCreateOrEditDto dto
  ) {
    return chapterVideoService.edit(videoId, dto);
  }

  @DeleteMapping("{chapterId}/video/{videoId}")
  public ApplicationResult<Object> delelte(@PathVariable Long videoId) {
    return chapterVideoService.delete(videoId);
  }
}
