package edu.gzhu.guli.back.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import edu.gzhu.guli.back.service.course.video.ChapterVideoService;
import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.core.oss.OSSService;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("api/aliyun")
@AllArgsConstructor
public class AliyunController {
  final ChapterVideoService chapterVideoService;
  final OSSService ossService;

  @PostMapping("video")
  public ApplicationResult<String> uploadVideo(MultipartFile file) {
    return chapterVideoService.upload(file);
  }

  @GetMapping("video/{sourceId}/auth")
  public ApplicationResult<String> getVideoAuth(@PathVariable String sourceId) {
    return chapterVideoService.getVideoAuth(sourceId);
  }

  @PostMapping("image")
  public ApplicationResult<String> uploadImage(MultipartFile file) {
    String fileName = ossService.uploadFile(file, "image");
    String url = ossService.getSingeNatureUrl(fileName);
    return ApplicationResult.succese(url);
  }
}
