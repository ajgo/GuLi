package edu.gzhu.guli.back.service.admin;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class AdminCreateOrEditDto {
  /*
   * 用户名
   */
  @NotNull
  @NotBlank
  private String username;

  /*
   * 密码
   */
  @NotNull
  @NotBlank
  private String password;

  /*
   * 姓名
   */
  @NotNull
  @NotBlank
  private String name;
}
