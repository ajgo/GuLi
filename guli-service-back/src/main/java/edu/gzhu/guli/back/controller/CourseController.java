package edu.gzhu.guli.back.controller;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.gzhu.guli.back.service.course.CourseCreateOrEditDto;
import edu.gzhu.guli.back.service.course.CourseReadDto;
import edu.gzhu.guli.back.service.course.CourseService;
import edu.gzhu.guli.core.common.ApplicationResult;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("api/course")
public class CourseController {
  final CourseService courseService;
  
  @GetMapping
  public ApplicationResult<Page<CourseReadDto>> loadAllPage(
    @RequestParam(defaultValue = "0") int page,
    @RequestParam(defaultValue = "10") int size
  ) {
    return courseService.loadAllPage(page, size);
  }

  @GetMapping("{courseId}")
  public ApplicationResult<CourseReadDto> loadDetails(Long courseId) {
    return courseService.loadDetails(courseId);
  }

  @PostMapping
  public ApplicationResult<CourseReadDto> create(@Valid @RequestBody CourseCreateOrEditDto dto) {
    return courseService.create(dto);
  }

  @PutMapping("{courseId}")
  public ApplicationResult<CourseReadDto> edit(@PathVariable Long courseId, @Valid @RequestBody CourseCreateOrEditDto dto) {
    return courseService.edit(courseId, dto);
  }

  @DeleteMapping("{courseId}")
  public ApplicationResult<Object> delete(@PathVariable Long courseId) {
    return courseService.delete(courseId);
  }

  @PatchMapping("{courseId}/teachr/{teacherId}")
  public ApplicationResult<Object> assignTeacherToCourse(@PathVariable Long teacherId, @PathVariable Long courseId) {
    return courseService.assignTeacher(teacherId, courseId);
  }
}
