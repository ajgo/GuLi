package edu.gzhu.guli.back.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.gzhu.guli.back.service.permission.PermissionCascadeDto;
import edu.gzhu.guli.back.service.permission.PermissionCreateOrEditDto;
import edu.gzhu.guli.back.service.permission.PermissionService;
import edu.gzhu.guli.core.common.ApplicationResult;
import lombok.AllArgsConstructor;

@RequestMapping("api/permission")
@RestController
@AllArgsConstructor
public class PermissionController {

  final PermissionService permissionService;

  @GetMapping("cascade")
  public ApplicationResult<List<PermissionCascadeDto>> cascade() {
    return permissionService.cascade();
  }

  @PostMapping
  public ApplicationResult<Object> create(@Valid @RequestBody PermissionCreateOrEditDto dto) {
    return permissionService.create(dto);
  }

  @PutMapping("{permissionId}")
  public ApplicationResult<Object> edit(
    @PathVariable Long permissionId,
    @Valid @RequestBody PermissionCreateOrEditDto dto
  ) {
    return permissionService.edit(permissionId, dto);
  }

  @DeleteMapping("{permissionId}")
  public ApplicationResult<Object> edit(
    @PathVariable Long permissionId
  ) {
    return permissionService.delete(permissionId);
  }
}
