package edu.gzhu.guli.back.service.course.video;


import lombok.Data;

@Data
public class VideoReadDto {

  private Long id;

  /*
   * 视频标题
   */
  private String title;

  /*
   * 阿里云视频上传后返回的ID 
   */
  private String sourceId;

  /*
   * 
   */
  private String orginalName;

  /*
   * 视频观看次数
   */
  private int playCount;

  /*
   * 视频时长
   */
  private int duration;
}
