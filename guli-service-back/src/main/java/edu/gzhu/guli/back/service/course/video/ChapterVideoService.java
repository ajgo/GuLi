package edu.gzhu.guli.back.service.course.video;

import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.core.entity.Chapter;
import edu.gzhu.guli.core.entity.Video;
import edu.gzhu.guli.core.repository.ChapterRepository;
import edu.gzhu.guli.core.repository.VideoRepository;
import edu.gzhu.guli.core.video.VideoService;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ChapterVideoService {
  final ModelMapper modelMapper;
  final ChapterRepository chapterRepository;
  final VideoRepository videoRepository;
  final VideoService videoService;

  // 上传课程视频返回阿里云视频ID
  public ApplicationResult<String> upload(MultipartFile file) {
    String sourceId = videoService.upload(file);
    return ApplicationResult.succese(sourceId);
  }

  // 
  public ApplicationResult<String> getVideoAuth(String sourceId) {
    String playAuth = videoService.getVideoAuth(sourceId);
    if (playAuth == null) return ApplicationResult.failure("获取播放凭证失败");
    return ApplicationResult.succese(playAuth);
  }

  // 创建视频，并绑定章节
  public ApplicationResult<VideoReadDto> create(Long chapterId, @Valid @RequestBody VideoCreateOrEditDto dto) {
    String sourceId = dto.getSource();
    boolean isValid = videoService.validSourceId(sourceId);
    if (!isValid)
      return ApplicationResult.failure("无效的source id");
    Optional<Chapter> check = chapterRepository.findById(chapterId);
    if (!check.isPresent())
      return ApplicationResult.failure("未找到目标章节");
    Chapter chapter = check.get();
    Video video = modelMapper.map(dto, Video.class);
    video.setChapter(chapter);
    video = videoRepository.save(video);
    VideoReadDto data = modelMapper.map(video, VideoReadDto.class);
    return ApplicationResult.succese(data);
  }

  // 编辑视频信息
  public ApplicationResult<VideoReadDto> edit(Long videoId, VideoCreateOrEditDto dto) {
    String sourceId = dto.getSource();
    boolean isValid = videoService.validSourceId(sourceId);
    if (!isValid)
      return ApplicationResult.failure("无效的source id");
    Optional<Video> check = videoRepository.findById(videoId);
    if (!check.isPresent())
      return ApplicationResult.failure("未找到对应的视频");
    Video video = check.get();
    modelMapper.map(dto, video);
    video = videoRepository.save(video);
    VideoReadDto data = modelMapper.map(video, VideoReadDto.class);
    return ApplicationResult.succese(data);
  }

  // 删除视频
  public ApplicationResult<Object> delete(Long videoId) {
    Optional<Video> check = videoRepository.findById(videoId);
    if (!check.isPresent())
      return ApplicationResult.failure("未找到对应的视频");
    Video video = check.get();
    String sourceId = video.getSourceId();
    if (!videoService.delete(sourceId)){
      return ApplicationResult.failure("删除视频失败");
    }
    videoRepository.delete(video);
    return ApplicationResult.succese();
  }

}
