package edu.gzhu.guli.back.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.gzhu.guli.back.service.course.CourseReadDto;
import edu.gzhu.guli.back.service.subject.SubjectCascadeDto;
import edu.gzhu.guli.back.service.subject.SubjectCreateOrEditDto;
import edu.gzhu.guli.back.service.subject.SubjectReadDto;
import edu.gzhu.guli.back.service.subject.SubjectService;
import edu.gzhu.guli.core.common.ApplicationResult;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("api/subject")
@AllArgsConstructor
public class SubjectController {

  final SubjectService subjectService;

  @GetMapping
  public ApplicationResult<List<SubjectCascadeDto>> loadAll () {
    return subjectService.loadAll();
  }

  @GetMapping("{subjectId}/course")
  public ApplicationResult<Page<CourseReadDto>> loadAllCourseOfSubject(
    @PathVariable Long subjectId,
    @RequestParam(defaultValue = "0") int page,
    @RequestParam(defaultValue = "10") int size
  ) {
    return subjectService.loadDetails(subjectId, page, size);
  }

  @PostMapping
  public ApplicationResult<SubjectReadDto> create(@Valid @RequestBody SubjectCreateOrEditDto dto) {
    return subjectService.create(dto);
  }

  @PutMapping("{subjectId}")
  public ApplicationResult<SubjectReadDto> edit(@PathVariable Long subjectId,
      @Valid @RequestBody SubjectCreateOrEditDto dto) {
    return subjectService.edit(subjectId, dto);
  }

  @DeleteMapping("{subjectId}")
  public ApplicationResult<Object> delete(@PathVariable Long subjectId) {
    return subjectService.delete(subjectId);
  }

  @PatchMapping("{subjectId}/course{courseId}")
  public ApplicationResult<Object> addCourseToSubject(
      @PathVariable Long subjectId, @PathVariable Long courseId) {
    return subjectService.addCourseToSubject(subjectId, courseId);
  }

}
