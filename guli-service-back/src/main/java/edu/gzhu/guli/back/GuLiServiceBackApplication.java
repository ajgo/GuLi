package edu.gzhu.guli.back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("edu.gzhu.guli.core.entity")
@EnableJpaRepositories("edu.gzhu.guli.core.repository")
@ComponentScan(basePackages = {"edu.gzhu.guli.core", "edu.gzhu.guli.back"})
public class GuLiServiceBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(GuLiServiceBackApplication.class, args);
	}

}
