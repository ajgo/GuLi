package edu.gzhu.guli.back.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.gzhu.guli.back.service.banner.BannerCreateOrEditDto;
import edu.gzhu.guli.back.service.banner.BannerReadDto;
import edu.gzhu.guli.back.service.banner.BannerService;
import edu.gzhu.guli.core.common.ApplicationResult;
import lombok.AllArgsConstructor;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
@RequestMapping("api/banner")
@AllArgsConstructor
public class BannerController {

  final BannerService bannerService;

  @GetMapping
  public ApplicationResult<List<BannerReadDto>> loadAllBanner() {
    return bannerService.loadAll();
  }

  @PostMapping
  public ApplicationResult<BannerReadDto> create(@Valid @RequestBody BannerCreateOrEditDto dto) {
    return bannerService.create(dto);
  }

  @PutMapping(value = "{bannerId}")
  public ApplicationResult<BannerReadDto> edit(@PathVariable Long bannerId,
      @Valid @RequestBody BannerCreateOrEditDto dto) {
    return bannerService.edit(bannerId, dto);
  }

  @DeleteMapping(value = "{bannerId}")
  public ApplicationResult<Object> delete(@PathVariable Long bannerId) {
    return bannerService.delete(bannerId);
  }
}
