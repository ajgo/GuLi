package edu.gzhu.guli.back.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.gzhu.guli.back.service.statistics.StatisticsAllDto;
import edu.gzhu.guli.back.service.statistics.StatisticsService;
import edu.gzhu.guli.core.common.ApplicationResult;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("api/statistics")
public class StatisticsController {
  
  final StatisticsService statisticsService;

  @GetMapping("all")
  public ApplicationResult<List<StatisticsAllDto>> getTotal() {
    return statisticsService.getAll();
  }
}
