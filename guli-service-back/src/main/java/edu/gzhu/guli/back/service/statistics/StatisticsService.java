package edu.gzhu.guli.back.service.statistics;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import edu.gzhu.guli.core.common.ApplicationResult;
import edu.gzhu.guli.core.entity.DailyStatistics;
import edu.gzhu.guli.core.repository.DailyStatisticsRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class StatisticsService {
  final DailyStatisticsRepository statisticsRepository;
  final ModelMapper modelMapper;

  public ApplicationResult<List<StatisticsAllDto>> getAll() {
    List<DailyStatistics> payload = statisticsRepository.findAll();
    List<StatisticsAllDto> data = payload.stream()
      .map(it -> modelMapper.map(it, StatisticsAllDto.class)).collect(Collectors.toList());
    return ApplicationResult.succese(data);
  }
}
