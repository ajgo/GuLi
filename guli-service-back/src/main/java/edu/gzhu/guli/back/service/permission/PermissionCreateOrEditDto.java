package edu.gzhu.guli.back.service.permission;

import lombok.Data;

@Data
public class PermissionCreateOrEditDto {
    /*
   * 父级权限菜单ID
   */
  private Long pid;
  
  /*
  * 权限代码
   */
  private int code;
  
  /*
  * 权限路径
  */
  private String url;
  
  /*
  * 权限名称
  */
  private String name;
  
  /*
  * 权限级别
  */
  private int level;
}
