package edu.gzhu.guli.back.service.subject;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class SubjectReadDto {

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private Long id;

   /*
   * 父级主题ID
   */
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private Long pid;

  /*
   * 主题名称
   */
  private String name;

  /*
   * 级别
   */
  private int level;
  
  /*
  * 排序规则
  */
  private int sort;
}
