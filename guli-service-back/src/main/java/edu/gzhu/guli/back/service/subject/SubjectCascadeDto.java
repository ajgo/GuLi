package edu.gzhu.guli.back.service.subject;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import edu.gzhu.guli.core.entity.Subject;
import lombok.Data;

@Data
public class SubjectCascadeDto {

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private Long id;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private Long pid;

  private boolean top;

  private String name;

  private int level;

  private int sort;

  private int courseNumber;

  private LocalDateTime createTime;

  private List<SubjectCascadeDto> children;

  public static SubjectCascadeDto father(Subject top) {
    SubjectCascadeDto topDto = new SubjectCascadeDto();
    topDto.setId(top.getId());
    topDto.setName(top.getName());
    topDto.setLevel(top.getLevel());
    topDto.setSort(top.getSort());
    topDto.setPid(top.getPid());
    topDto.setCreateTime(top.getCreateTime());
    topDto.setTop(true);
    topDto.setCourseNumber(top.getCourses().size());
    topDto.setChildren(new ArrayList<>());
    return topDto;
  }

  public static SubjectCascadeDto child(Subject child) {
    SubjectCascadeDto childDto = new SubjectCascadeDto();
    childDto.setId(child.getId());
    childDto.setName(child.getName());
    childDto.setLevel(child.getLevel());
    childDto.setSort(child.getSort());
    childDto.setPid(child.getPid());
    childDto.setCreateTime(child.getCreateTime());
    childDto.setTop(false);
    childDto.setCourseNumber(child.getCourses().size());
    return childDto;
  }
}
