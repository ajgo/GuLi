package edu.gzhu.guli.back.service.course;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import edu.gzhu.guli.back.service.course.chapter.ChapterReadDto;
import edu.gzhu.guli.back.service.subject.SubjectReadDto;
import edu.gzhu.guli.back.service.teacher.TeacherReadDto;
import lombok.Data;

@Data
public class CourseReadDto {

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private Long id;

  /*
   * 课程名
   */
  private String title;

  /*
   * 课程描述 （可以考虑markdown格式）
   */
  private String description;

  /*
   * 课程价格
   */
  private float price = 0.0f;

  /*
   * 课程数量
   */
  private int lessonNum;

  /*
   * 课程封面图片url
   */
  private String cover;

  /*
   * 课程的购买数量
   */
  private int buyCount;

  /*
   * 课程的观看数量
   */
  private int viewCount;

  /*
   * 课程状态（0代表为草稿，1代表为已发布）
   */
  private boolean status;

  /*
   * 课程讲师
   */
  private TeacherReadDto teacher;

  /*
   * 课程讲师
   */
  private SubjectReadDto topSubject;

  /*
   * 课程讲师
   */

  private SubjectReadDto subject;

  /*
   * 课程的章节
   */
  private List<ChapterReadDto> chapters = new ArrayList<>();
}
