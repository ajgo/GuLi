## 单调栈的应用

```cpp
class Solution
{
public:
    vector<int> dailyTemperatures(vector<int> temperatures)
    {
        stack<int> st;
        vector<int> result(temperatures.size());
        for (int i = 0; i < temperatures.size(); i++)
        {
            while (!st.empty() && temperatures[i] > temperatures[st.top()])
            {
                int j = st.top();
                st.pop();
                result[j] = i - j;
            }
            st.push(i);
        }
        return result;
    }
};
```



## 克隆图

```c++
class Solution
{
public:
    Node *cloneGraph(Node *node)
    {
        if (node == nullptr)
            return nullptr;
        auto it = map.find(node);
        if (it != map.end())
        {
            return it->second;
        }
        Node *theNode = new Node(node->val);
        map[node] = theNode;
        for (int i = 0; i < node->neighbors.size(); i++)
        {
            Node *neighbor = cloneGraph(node->neighbors[i]);
            theNode->neighbors.push_back(neighbor);
        }
        return theNode;
    }

private:
    unordered_map<Node *, Node *> map;
};
```

## 克隆随机链表

```python
class Solution:
    def copyRandomList(self, head: 'Optional[Node]') -> 'Optional[Node]':
        if not head:
            return None
        current, dummy = head, Node(-1)
        p = dummy
        factory: Dict[Node, Node] = {}
        while current:
            newNode = Node(current.val)
            factory[current] = newNode
            p.next = newNode
            p = p.next
            current = current.next

        current, p = head, dummy.next
        while current:
            p.next = factory[current.next] if current.next else None
            p.random = factory[current.random] if current.random else None
            current = current.next
        return dummy.next
```



## 字典树

````java
public class Trie {
    class Node {
        Map<Character, Node> next = new HashMap<>();
        boolean end = false;
    }

    private Node root;

    public Trie() {
        this.root = new Node();
    }

    public void insert(String word) {
        Node curNode = root;
        for (int i = 0; i < word.length(); i++) {
            Character c = word.charAt(i);
            curNode = curNode.next.computeIfAbsent(c, (key) -> new Node());
        }
        curNode.end = true;
    }

    public boolean search(String word) {
        return searchBaseOnEnd(word, true);
    }

    public boolean startsWith(String prefix) {
        return searchBaseOnEnd(prefix, false);
    }

    private boolean searchBaseOnEnd(String word, boolean requireEnd) {
        Node curNode = root;
        for (int i = 0; i < word.length(); i++) {
            Character c = word.charAt(i);
            if (curNode.next.get(c) == null)
                return false;
            curNode = curNode.next.get(c);
        }
        if (requireEnd) {
            return curNode.end;
        }
        return true;
    }
}

````

## 格雷编码

| n =0 | n=1  | n=2  | n=3  |
| ---- | ---- | ---- | ---- |
| 0    | 0    | 00   | 000  |
|      | 1    | 01   | 001  |
|      |      | 11   | 011  |
|      |      | 10   | 010  |
|      |      |      | 110  |
|      |      |      | 111  |
|      |      |      | 101  |
|      |      |      | 100  |

```java
// 镜像对称法
class Solution {
    public List<Integer> grayCode(int n) {
        List<Integer> res = new ArrayList<>(1 << n);
        res.add(0);
        for (int i = 0; i < n; i++) {
            int len = res.size();
            int head = 1 << i;
            for (int k = len - 1; k >= 0; k--) {
                int cur = res.get(k);
                res.add(cur | head);
            }
        }
        return res;
    }
}
```

## 字符串相乘

```java
class Solution {
    public String multiply(String num1, String num2) {
        int len1 = num1.length(), len2 = num2.length();
        int[] res = new int[len1 + len2];
        char[] maxLen, minLen;
        maxLen = len1 >= len2 ? num1.toCharArray() : num2.toCharArray();
        minLen = len1 >= len2 ? num2.toCharArray() : num1.toCharArray();
        for (int i = minLen.length - 1; i >= 0; i--) {
            int x = minLen[i] - '0';
            for (int j = maxLen.length - 1; j >= 0; j--) {
                int y = maxLen[j] - '0';
                res[i + j + 1] += x * y;
            }
        }

        int carry = 0;
        for (int k = len1 + len2 - 1; k >= 0; k--) {
            int n = res[k] + carry;
            res[k] = n % 10;
            carry = n / 10;
        }
        int k = 0;
        while (k < len1 + len2 && res[k] == 0)
            k++;
        StringBuilder sb = new StringBuilder();
        for (int i = k; i < len1 + len2; i++) {
            sb.append(res[i]);
        }
        return sb.length() == 0 ? "0" : sb.toString();
    }
}
```

## 逃离大迷宫（有限步数BFS）

```java
class Solution {

    class Node {
        int x, y;

        Node(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int hashCode() {
            return (x << 8) | y;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Node))
                return false;
            Node node = (Node) obj;
            return node.x == x && node.y == y;
        }

    }

    final int[][] direction = { { 1, 0 }, { -1, 0 }, { 0, 1 }, { 0, -1 } };
    final int N = 1000000;

    static final int BOLOCKED = -1;
    static final int FOUND = 0;
    static final int VAILD = 1;

    public boolean isEscapePossible(int[][] blocked, int[] source, int[] target) {
        if (blocked.length < 2)
            return true;

        Set<Node> sBolcked = new HashSet<>();
        for (int i = 0; i < blocked.length; i++) {
            sBolcked.add(new Node(blocked[i][0], blocked[i][1]));
        }
        int res = check(blocked, source, target, sBolcked);
        if (res == FOUND)
            return true;
        if (res == BOLOCKED)
            return false;

        res = check(blocked, target, source, sBolcked);

        return res == VAILD;
    }

    private int check(int[][] blocked, int[] source, int[] target, Set<Node> sBolcked) {
        Set<Node> sVisited = new HashSet<>();
        Queue<Node> que = new ArrayDeque<>();
        Node start = new Node(source[0], source[1]);
        Node from = new Node(target[0], target[1]);
        int count = (blocked.length - 1) * blocked.length / 2;
        sVisited.add(start);
        que.offer(start);
        while (!que.isEmpty() && count > 0) {
            Node current = que.poll();
            for (int i = 0; i < 4; i++) {
                int dx = current.x + direction[i][0];
                int dy = current.y + direction[i][1];
                Node newNode = new Node(dx, dy);
                if (dx < 0 || dy < 0 || dx >= N || dy >= N || sBolcked.contains(newNode) || sVisited.contains(newNode))
                    continue;
                if (newNode.equals(from))
                    return FOUND;
                count--;
                sVisited.add(newNode);
                que.offer(newNode);
            }
        }
        if (count > 0)
            return BOLOCKED;
        return VAILD;
    }
}
```



## 01矩阵

```python
"""
输入：mat = [[0,0,0],[0,1,0],[1,1,1]]
输出：[[0,0,0],[0,1,0],[1,2,1]]

从所有为零的坐标开始BFS
"""
class Solution:
    def updateMatrix(self, mat: List[List[int]]) -> List[List[int]]:
        n, m = len(mat), len(mat[0])
        que: Deque[Tuple[int, int]] = deque(
            [(i, j) for i in range(n) for j in range(m) if mat[i][j] == 0])
        dist = [[0]*m for _ in range(n)]

        while que:
            x, y = que.popleft()
            for nx, ny in [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]:
                if 0 <= nx < n and 0 <= ny < m and mat[nx][ny] == 1:
                    que.append((nx, ny))
                    mat[nx][ny] = 0
                    dist[nx][ny] = dist[x][y] + 1

        return dist
```



## 最长递增子序列（LIS）

### 解法一👉普通动态规划

$\text 状态转移方程：dp[i]=\max\limits_{0<j<i,0<i<n}(dp[j]+1,dp[i])$

$\text dp[i]表示以nums[i]结尾的子序列的长度$

```python
# 时间复杂度都为O(n^2)
class Solution:
    def lengthOfLIS(self, nums: List[int]) -> int:
        count = len(nums)
        dp = [1 for _ in nums]
        maxLen = 1
        for i in range(count):
            for j in range(i):
                if nums[i] > nums[j]:
                    dp[i] = max(dp[j] + 1, dp[i])
            maxLen = max(maxLen, dp[i])
        return maxLen
```

### 解法二👉贪心+ 二分查找  

$$
\begin{align}
&dp[i]表示长度为i的子序列的最小的结尾元素 \\
&dp[i] >= dp[i-1], dp为单调递增数列 \\
&使用二分查找,循环结束时有left=right\ and\ dp[left-1] < nums[i] <= dp[left] \\
&dp[left]=nums[i],更新dp
\end{align}
$$

```python
"""
e.g.
初始状态
nums = [10,9,2,5,3,7,21,18]
dp = [0,0,0,0,0,0,0,0]

nums = [10,9,2,5,3,7,21,18] nums[i] = 10
dp = [10,0,0,0,0,0,0,0]

nums = [10,9,2,5,3,7,21,18] nums[i] = 9
dp = [9,0,0,0,0,0,0,0]

nums = [10,9,2,5,3,7,21,18] nums[i] = 2
dp = [2,0,0,0,0,0,0,0]

nums = [10,9,2,5,3,7,21,18] nums[i] = 5
dp = [2,5,0,0,0,0,0,0]

nums = [10,9,2,5,3,7,21,18] nums[i] = 3
dp = [2,3,0,0,0,0,0,0]

nums = [10,9,2,5,3,7,21,18] nums[i] = 7
dp = [2,3,7,0,0,0,0,0]

nums = [10,9,2,5,3,7,21,18] nums[i] = 21
dp = [2,3,7,21,0,0,0,0]

nums = [10,9,2,5,3,7,21,18] nums[i] = 18
dp = [2,3,7,18,0,0,0,0]

此时dpLen = 4
"""
class Solution:
    def lengthOfLIS(self, nums: List[int]) -> int:
        count = len(nums)
        dp, dpLen = [0] * count, 0
        for num in nums:
            left, right = 0, dpLen
            while left < right:
                mid = (left + right) >> 1
                if dp[mid] >= num:
                    right = mid
                else:
                    left = mid + 1
            dp[left] = num
            if left == dpLen:
                dpLen += 1
        return dpLen
```



## 丑数Ⅱ

### 解法一👉最小堆+哈希表

初始时堆为空。首先将最小的丑数 1 加入堆。

每次取出堆顶元素 $x$，则 $x$ 是堆中最小的丑数，由于$2x,3x,5x$ 也是丑数，因此将 $2x,3x,5x$ 加入堆。

上述做法会导致堆中出现重复元素的情况。为了避免重复元素，可以使用哈希集合去重，避免相同元素多次加入堆。

在排除重复元素的情况下，第 n 次从最小堆中取出的元素即为第 n 个丑数。

```python
class Solution:
    def nthUglyNumber(self, n: int) -> int:
        facters = [2, 3, 5]
        heap = [1]
        seen = {1}
        ugly = 0
        for i in range(n):
            current = heapq.heappop(heap)
            for facter in facters:
                nxt = current * facter
                if nxt not in seen:
                    seen.add(nxt)
                    heapq.heappush(heap, nxt)
            ugly = current
        return ugly
```





### 解法一👉多路归并（多指针）

- 由丑数*2构成的有序数列 $1\ast2\ ,2\ast2\ ,3\ast2\ ,4\ast2\ ,5\ast2\ ,6\ast2\ ,7\ast2\ \cdots$
- 由丑数*3构成的有序数列 $1\ast3\ ,2\ast3\ ,3\ast3\ ,4\ast3\ ,5\ast3\ ,6\ast3\ ,7\ast3\ \cdots$
- 由丑数*5构成的有序数列 $1\ast5\ ,2\ast5\ ,3\ast5\ ,4\ast5\ ,5\ast5\ ,6\ast5\ ,7\ast5\ \cdots$

```python
### 用三个指针记录三个有序数列的排头坐标
class Solution:
    def nthUglyNumber(self, n: int) -> int:
        res = [1 for _ in range(n)]
        idx = [0, 0, 0]
        for i in range(1, n):
            n1, n2, n3 = 2 * res[idx[0]], 3 * res[idx[1]], 5 * res[idx[2]]
            target = min(n1, n2, n3)
            # 这里不可以使用if-else
            # 例如 6可以由2*3生成也可以由3*2生成
            # 将三个有序序列中的最小一位存入「已有丑数」序列，并将其下标后移
            if target == n1:
                idx[0] += 1
            if target == n2:
                idx[1] += 1
            if target == n3:
                idx[2] += 1
            res[i] = target
        return res
```



## 组合（回溯基础）

```python
class Solution:
    def combine(self, n: int, k: int) -> List[List[int]]:
        ans = []
        def backtrack(start: int, count: int, res: List[int]):
            if count == k:
                ans.append(res.copy())
                return
            # 剪枝操作
            if n - start + 1 < k - count:
                return
            for i in range(start, n + 1):
                res.append(i)
                backtrack(i + 1, count + 1, res)
                res.pop()
            return
        backtrack(1, 0, [])
        return ans
```

## 字母的排列（滑动窗口）

```python
"""
输入：s1 = "ab" s2 = "eidbaooo"
输出：true
解释：s2 包含 s1 的排列之一 ("ba").
"""
class Solution:
    def checkInclusion(self, s1: str, s2: str) -> bool:
        m, n = len(s1), len(s2)
        if m > n:
            return False
        pattern, windows = [0] * 26, [0] * 26
        for i in range(m):
            pattern[ord(s1[i]) - ord('a')] += 1
            windows[ord(s2[i]) - ord('a')] += 1
        for i in range(m, n):
            if pattern == windows:
                return True
            windows[ord(s2[i]) - ord('a')] += 1
            windows[ord(s2[i - m]) - ord('a')] -= 1
        return windows == pattern
```





# 常见二进制变换操作

   功能                   |          示例                      |       位运算
去掉最后一位           | (101101->10110)           | x shr 1
在最后加一个0         | (101101->1011010)         | x shl 1
在最后加一个1         | (101101->1011011)         | x shl 1+1
把最后一位变成1      | (101100->101101)          | x or 1
把最后一位变成0      | (101101->101100)          | x or 1-1
最后一位取反           | (101101->101100)          | x xor 1
把右数第k位变成1      | (101001->101101,k=3)      | x or (1 shl (k-1))
把右数第k位变成0      | (101101->101001,k=3)      | x and not(1 shl (k-1))
右数第k位取反         | (101001->101101,k=3)      | x xor (1 shl (k-1))
取末三位              | (1101101->101)            | x and 7
取末k位               | (1101101->1101,k=5)       | x and (1 shl k-1)
取右数第k位           | (1101101->1,k=4)          | x shr (k-1) and 1
把末k位变成1          | (101001->101111,k=4)      | x or (1 shl k-1)
末k位取反             | (101001->100110,k=4)      | x xor (1 shl k-1)
把右边连续的1变成0    | (100101111->100100000)    | x and (x+1)
把右起第一个0变成1    | (100101111->100111111)    | x or (x+1)
把右边连续的0变成1    | (11011000->11011111)      | x or (x-1)
取右边连续的1         | (100101111->1111)         | (x xor (x+1)) shr 1

# 柯朵莉树

# 树状数组

```python
from collections import defaultdict


# tree直接用dict 省去离散化步骤
class BIT1:
    """单点修改"""

    def __init__(self, n: int):
        self.size = n
        self.tree = defaultdict(int)

    @staticmethod
    def _lowbit(index: int) -> int:
        return index & -index

    def add(self, index: int, delta: int) -> None:
        if index <= 0:
            raise ValueError('index 必须是正整数')
        while index <= self.size:
            self.tree[index] += delta
            index += self._lowbit(index)

    def query(self, index: int) -> int:
        if index > self.size:
            index = self.size
        res = 0
        while index > 0:
            res += self.tree[index]
            index -= self._lowbit(index)
        return res

    def sumRange(self, left: int, right: int) -> int:
        return self.query(right) - self.query(left - 1)


class BIT2:
    """范围修改"""

    def __init__(self, n: int):
        self.size = n
        self._tree1 = defaultdict(int)
        self._tree2 = defaultdict(int)

    @staticmethod
    def _lowbit(index: int) -> int:
        return index & -index

    def add(self, left: int, right: int, delta: int) -> None:
        """闭区间[left, right]加delta"""
        self._add(left, delta)
        self._add(right + 1, -delta)

    def query(self, left: int, right: int) -> int:
        """闭区间[left, right]的和"""

        return self._query(right) - self._query(left - 1)

    def _add(self, index: int, delta: int) -> None:
        if index <= 0:
            raise ValueError('index 必须是正整数')

        rawIndex = index
        while index <= self.size:
            self._tree1[index] += delta
            self._tree2[index] += (rawIndex - 1) * delta
            index += self._lowbit(index)

    def _query(self, index: int) -> int:
        if index > self.size:
            index = self.size

        rawIndex = index
        res = 0
        while index > 0:
            res += rawIndex * self._tree1[index] - self._tree2[index]
            index -= self._lowbit(index)
        return res
```

# 背包问题

```java
class Solution {
  public int func(int[] nums) {
    int n = nums.length;
    List<Integer> me = new ArrayList<>();
    List<Integer> other = new ArrayList<>();
    Set<Integer> meSet = new HashSet<>();
    List<Integer> current = new ArrayList<>();
    boolean find = false;
    for (int state = (1 << n) - 1; state > 0; state--) {
      current.clear();
      int sum = 0;
      for (int i = 0; i < n; i++) if (((state >> i) & 1 ) == 1) {
        current.add(nums[i]);
        sum += nums[i];
      }
      if ((sum & 1) == 1) continue;
      int numCount = current.size(), capcity = sum / 2;
      int[][] weights = new int[numCount + 1][capcity + 1];
      for (int i = 1; i <= numCount; i++) {
        int c = current.get(i - 1);
        for (int j = 0; j <= capcity; j++) {
          weights[i][j] = weights[i - 1][j];
          if (j >= c){
            weights[i][j] = Math.max(weights[i][j], weights[i - 1][j - c] + c);
          } 
        }
      }
      if (weights[numCount][capcity] != capcity) continue;
      
      System.out.println(current);
      for (int i = 1; i <= numCount; i++) 			System.out.println(Arrays.toString(weights[i]));

      int cap = capcity;
      for (int i = numCount; i >= 1; i--) {
        int c = current.get(i - 1);
        if (cap >= c && weights[i][cap] == weights[i - 1][cap - c] + c) {
          cap -= c;
          meSet.add(i - 1);
        } 
      }
      find = true;
      break;
    }
    if (!find){
      System.out.println("Not find solution");
      return 0;
    }

    for (int i = 0; i < current.size(); i++) {      
      if(!meSet.contains(i)) {
       other.add(current.get(i));
      } else {
        me.add(current.get(i));
      }
    }
    System.out.println(me);
    System.out.println(other);
    int mesum = 0;
    int othersum = 0;
    for(int i = 0; i < me.size(); i++) {
      mesum += me.get(i);
    }
    for(int i = 0; i < other.size(); i++) {
      othersum += other.get(i);
    }
    if (mesum != othersum) throw new RuntimeException();
    return 0;
  }
}
```

