package edu.gzhu.guli.back;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.vod.model.v20170321.GetVideoInfoRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoInfoResponse;

import edu.gzhu.guli.core.oss.OSSService;
import edu.gzhu.guli.core.video.VideoConfiguration;
import edu.gzhu.guli.core.video.VideoService;

@SpringBootTest
class GuLiServiceBackApplicationTests {

	@Autowired
	OSSService ossService;

	@Autowired
	VideoService videoService;

	@Test
	void contextLoads() {

	}

	// @Test
	void loadFile() throws IOException {
		ClassPathResource classPathResource = new ClassPathResource("test.flv");
		File f = classPathResource.getFile();
		MultipartFile file = new MockMultipartFile(f.getName(), f.getName(), null, classPathResource.getInputStream());
		String videoId = videoService.upload(file);
		System.out.println(videoId);
	}

	@Test
	void loadVideoInfoBySourceId() {
		DefaultAcsClient client = VideoConfiguration.initVodClient();
		GetVideoInfoResponse response = new GetVideoInfoResponse();
		try {
			GetVideoInfoRequest request = new GetVideoInfoRequest();
			request.setVideoId("595b47376ae44615a971c1b388d36693");
			response = client.getAcsResponse(request);
			System.out.print("Title = " + response.getVideo().getTitle() + "\n");
			System.out.print("Description = " + response.getVideo().getDescription() + "\n");
		} catch (Exception e) {
			System.out.print("ErrorMessage = " + e.getLocalizedMessage());
		}
		System.out.print("RequestId = " + response.getRequestId() + "\n");
	}
}
