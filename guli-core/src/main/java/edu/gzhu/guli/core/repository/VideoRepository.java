package edu.gzhu.guli.core.repository;

import org.springframework.stereotype.Repository;

import edu.gzhu.guli.core.entity.Video;
import edu.gzhu.guli.core.repository.support.BaseRepository;


@Repository
public interface VideoRepository extends BaseRepository<Video, Long> {
  
}
