package edu.gzhu.guli.core.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Role extends BaseEntity<Long> {
  /*
   * 角色名称
   * 示例： ADMIN, ROOT, OTHER
   */
  @Column(nullable = false, unique = true)
  private String name;

  /*
   * 对角色的功能描述
   */
  private String description;

  @OneToMany(mappedBy = "role", cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST })
  private List<Admin> members;

  @Builder.Default
  @OneToMany(mappedBy = "role", cascade = { CascadeType.ALL })
  private List<RolePermission> permissions = new ArrayList<>();
}
