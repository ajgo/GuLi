package edu.gzhu.guli.core.repository;

import org.springframework.stereotype.Repository;

import edu.gzhu.guli.core.entity.Chapter;
import edu.gzhu.guli.core.repository.support.BaseRepository;


@Repository
public interface ChapterRepository extends BaseRepository<Chapter, Long> {
  
}
