package edu.gzhu.guli.core.repository.support;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;

public class IdGenerator implements IdentifierGenerator {

  private SnowflakeIdWorker idWorker;

  public IdGenerator() {
    idWorker = new SnowflakeIdWorker(0, 0);
  }

  @Override
  public Serializable generate(SharedSessionContractImplementor session, Object object)
      throws HibernateException {
    return idWorker.nextId();
  }
}
