package edu.gzhu.guli.core.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Permission extends BaseEntity<Long> {
  /*
   * 父级权限菜单ID
   */
  @Column(nullable = false)
  private Long pid;
  
  /*
  * 权限代码
   */
  @Column(nullable = false, unique = true)
  private int code;
  
  /*
  * 权限路径
  */
  private String url;
  
  /*
  * 权限名称
  */
  @Column(nullable = false)
  private String name;
  
  /*
  * 权限级别
  */
  @Column(nullable = false)
  private int level;

  /*
   * 拥有该条权限的角色
   */
  @OneToMany(mappedBy = "permission", cascade = { CascadeType.ALL })
  private List<RolePermission> roles = new ArrayList<>();
}
