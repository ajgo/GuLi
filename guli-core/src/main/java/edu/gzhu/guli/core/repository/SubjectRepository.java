package edu.gzhu.guli.core.repository;

import org.springframework.stereotype.Repository;

import edu.gzhu.guli.core.entity.Subject;
import edu.gzhu.guli.core.repository.support.BaseRepository;

import java.util.List;
import java.util.Optional;


@Repository
public interface SubjectRepository extends BaseRepository<Subject, Long> {

    List<Subject> findByLevelOrderBySortAsc(int level);

    List<Subject> findByLevelAndPidOrderBySortAsc(int level,long pid);

    Optional<Subject> findByName(String name);
}
