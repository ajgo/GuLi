package edu.gzhu.guli.core.video;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.profile.DefaultProfile;

@Configuration
public class VideoConfiguration {

  public static String ACCESS_KEY_ID;

  public static String ACCESS_KEY_SECRET;

  public static String API_REGION_ID = "cn-shanghai";

  @Value("${aliyun.access-key-id}")
  public void setAccessKeyId(String accessKeyId) {
    VideoConfiguration.ACCESS_KEY_ID = accessKeyId;
  }

  @Value("${aliyun.access-key-secret}")
  public void setAccessKeySecret(String accessKeySecret) {
    VideoConfiguration.ACCESS_KEY_SECRET = accessKeySecret;
  }

  public static DefaultAcsClient initVodClient() {
    DefaultProfile profile = DefaultProfile.getProfile(API_REGION_ID, ACCESS_KEY_ID, ACCESS_KEY_SECRET);
    DefaultAcsClient client = new DefaultAcsClient(profile);
    return client;
  }
}
