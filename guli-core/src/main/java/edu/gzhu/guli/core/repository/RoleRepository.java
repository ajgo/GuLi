package edu.gzhu.guli.core.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import edu.gzhu.guli.core.entity.Role;
import edu.gzhu.guli.core.repository.support.BaseRepository;


@Repository
public interface RoleRepository extends BaseRepository<Role, Long> {
  Optional<Role> findByName(String name);
}
