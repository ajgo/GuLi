package edu.gzhu.guli.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Comment extends BaseEntity<Long> {

  /*
   * 评论用户的用户名
   */
  @Column(nullable = false)
  private String name;

  /*
   * 评论用户的头像url
   */
  @Column(columnDefinition = "text")
  private String avatar;

  /*
   * 评论的内容
   */
  @Column(nullable = false)
  private String content;

  /*
   * 给课程的评分
   */
  private int score = 5;

  /*
   * 评论属于的课程
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "course_id", nullable = false)
  private Course course;
  
  /*
   * 发布评论的用户
   */
  @ManyToOne
  @JoinColumn(name = "member_id", nullable = false)
  private Member member;
}
