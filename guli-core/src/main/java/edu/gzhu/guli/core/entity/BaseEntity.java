package edu.gzhu.guli.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners({ AuditingEntityListener.class })
public class BaseEntity<ID extends Serializable>
    implements Persistable<ID> {

  @Id
  @GenericGenerator(name = "idGenerator", strategy = "edu.gzhu.guli.core.repository.support.IdGenerator")
  @GeneratedValue(generator = "idGenerator")
  private ID id;

  @CreatedDate
  private LocalDateTime createTime;
  
  @LastModifiedDate
  private LocalDateTime modifyTime;

  public void setCreateTime(LocalDateTime createTime) {
    this.createTime = createTime;
  }

  public LocalDateTime getCreateTime() {
    return createTime;
  }


  public LocalDateTime getModifyTime() {
    return modifyTime;
  }

  public void setModifyTime(LocalDateTime modifyTime) {
    this.modifyTime = modifyTime;
  }

  @Override
  public ID getId() {
    return id;
  }

  public void setId(ID id) {
    this.id = id;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }
  
}