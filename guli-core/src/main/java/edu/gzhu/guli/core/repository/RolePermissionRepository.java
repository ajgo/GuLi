package edu.gzhu.guli.core.repository;

import org.springframework.stereotype.Repository;

import edu.gzhu.guli.core.entity.RolePermission;
import edu.gzhu.guli.core.repository.support.BaseRepository;


@Repository
public interface RolePermissionRepository extends BaseRepository<RolePermission, Long> {
  
}
