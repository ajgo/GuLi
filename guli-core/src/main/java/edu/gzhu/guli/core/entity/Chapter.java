package edu.gzhu.guli.core.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Chapter extends BaseEntity<Long> {

    /*
     * 章节名称
     */
    @Column(nullable = false)
    private String title;

    /*
     * 排序规则
     */
    private int sort = 0;

    /*
     * 是否可以预览
     */
    private boolean canReview = false;

    /*
     * 章节对应的课程
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id", nullable = false)
    private Course course;

    /*
     * 课程章节下的视频
     */
    @OneToOne(mappedBy = "chapter", cascade = {CascadeType.ALL})
    private Video video;

}
