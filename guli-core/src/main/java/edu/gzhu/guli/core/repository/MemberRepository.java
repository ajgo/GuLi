package edu.gzhu.guli.core.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import edu.gzhu.guli.core.entity.Member;
import edu.gzhu.guli.core.repository.support.BaseRepository;


@Repository
public interface MemberRepository extends BaseRepository<Member, Long> {
  Optional<Member> findByUsernameOrPhone(String username, String phone);

  Optional<Member> findByUsername(String username);
}
