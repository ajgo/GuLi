package edu.gzhu.guli.core.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class CourseOrder extends BaseEntity<Long> {

  /*
   * 订单编号
   */
  @Column(nullable = false)
  private String number;

  /*
   * 支付金额
   */
  private float totalFee;

  @Column(name = "course_id", insertable = false, updatable = false)
  private Long courseId;
  
  @Column(name = "member_id", insertable = false, updatable = false)
  private Long memberId;

  @JoinColumn(name = "member_id")
  @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
  private Member member;

  @JoinColumn(name = "course_id")
  @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
  private Course course;
}
