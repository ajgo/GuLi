package edu.gzhu.guli.core.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Teacher extends BaseEntity<Long> {

  /*
   * 姓名
   */
  @Column(nullable = false)
  private String name;
  
  /*
  * 介绍
  */
  @Column(nullable = false)
  private String introduce;
  
  /*
  * 职称
  */
  @Column(nullable = false)
  private String career;

  /*
   * 头像url
   */
  @Column(nullable = false, columnDefinition = "text")
  private String avatar;

  /*
   * 
   */
  private int level;

  /*
   * 排序方式
   */
  private int sort;

  /*
   * 讲师参与的课程
   */
  @OneToMany(mappedBy = "teacher", cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST })
  List<Course> courses = new ArrayList<>();
}
