package edu.gzhu.guli.core.repository.support;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.blinkfox.fenix.specification.FenixJpaSpecificationExecutor;

@NoRepositoryBean
public interface BaseRepository<T, ID>
    extends JpaRepository<T, ID>, FenixJpaSpecificationExecutor<T> {
}
