package edu.gzhu.guli.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Video extends BaseEntity<Long> {

  /*
   * 视频标题
   */
  @Column(nullable = false)
  private String title;

  /*
   * aliyun 视频点播ID
   */
  @Column(nullable = false)
  private String sourceId;

  /*
   * 
   */
  private String orginalName;

  /*
   * 视频观看次数
   */
  private int playCount;

  /*
   * 视频时长
   */
  private int duration;

  /*
   * 视频属于的课程章节
   */
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "chapter_id", nullable = false)
  private Chapter chapter;
}
