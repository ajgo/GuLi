package edu.gzhu.guli.core.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class RolePermission extends BaseEntity<Long> {

  /*
   * 角色ID
   */
  @Column(name = "role_id", insertable = false, updatable = false)
  private Long roleId;

  /*
   * 权限ID
   */
  @Column(name = "permission_id", insertable = false, updatable = false)
  private Long permissionId;

  @JoinColumn(name = "role_id")
  @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
  private Role role;

  @JoinColumn(name = "permission_id")
  @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
  private Permission permission;

}
