package edu.gzhu.guli.core.repository;

import org.springframework.stereotype.Repository;

import edu.gzhu.guli.core.entity.Teacher;
import edu.gzhu.guli.core.repository.support.BaseRepository;

@Repository
public interface TeacherRepository extends BaseRepository<Teacher, Long> {

    Teacher findAllByName(String name);
}
