package edu.gzhu.guli.core.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Subject extends BaseEntity<Long> {

  /*
   * 父级主题ID
   */
  @Column(nullable = false)
  private Long pid;

  /*
   * 主题名称
   */
  @Column(nullable = false, unique = true)
  private String name;

  /*
   * 级别
   */
  private int level;
  
  /*
  * 排序规则
  */
  private int sort;

  /*
   * 顶级主题下的所有课程
   */
  @OneToMany(mappedBy = "topSubject", cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST })
  private List<Course> allCourses = new ArrayList<>();

  /*
   * 子主题下的所有课程
   */
  @OneToMany(mappedBy = "subject", cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST })
  private List<Course> courses = new ArrayList<>();
}
