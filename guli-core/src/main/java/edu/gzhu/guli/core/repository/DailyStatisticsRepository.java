package edu.gzhu.guli.core.repository;

import org.springframework.stereotype.Repository;

import edu.gzhu.guli.core.entity.DailyStatistics;
import edu.gzhu.guli.core.repository.support.BaseRepository;

@Repository
public interface DailyStatisticsRepository extends BaseRepository<DailyStatistics, Long> {
  
}
