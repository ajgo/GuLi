package edu.gzhu.guli.core.repository;

import org.springframework.stereotype.Repository;

import edu.gzhu.guli.core.entity.Permission;
import edu.gzhu.guli.core.repository.support.BaseRepository;


@Repository
public interface PermissionRepository extends BaseRepository<Permission, Long>{
  
}
