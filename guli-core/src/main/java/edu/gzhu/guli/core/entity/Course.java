package edu.gzhu.guli.core.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Course extends BaseEntity<Long> {
  
  /*
   * 课程名
   */
  @Column(nullable = false, unique = true)
  private String title;
  
  /*
  * 课程描述 （可以考虑markdown格式）
  */
  @Column(nullable = false)
  private String description;
  
  /*
  * 课程价格
  */
  @Column(nullable = false)
  private float price = 0.0f;
  
  /*
  * 课程数量
  */
  @Column(nullable = false)
  private int lessonNum;
  
  /*
  * 课程封面图片url
  */
  @Column(columnDefinition = "text")
  private String cover;
  
  /*
  * 课程的购买数量
  */
  @Column(nullable = false)
  private int buyCount;
  
  /*
  * 课程的观看数量
  */
  @Column(nullable = false)
  private int viewCount;
  
  /*
  * 课程状态（0代表为草稿，1代表为已发布）
  */
  @Column(nullable = false)
  private boolean status;

  /*
   * 课程讲师
   */
  @ManyToOne
  @JoinColumn(name="teacher_id")
  private Teacher teacher;

  /*
   * 课程讲师
   */
  @ManyToOne
  @JoinColumn(name="subject_parent_id")
  private Subject topSubject;

  /*
   * 课程讲师
   */
  @ManyToOne
  @JoinColumn(name="subject_id")
  private Subject subject;

  /*
   * 课程的评论
   */
  @OneToMany(mappedBy = "course", cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST })
  private List<Comment> comments = new ArrayList<>();

  /*
   * 课程的章节
   */
  @OneToMany(mappedBy = "course", cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST })
  private List<Chapter> chapters = new ArrayList<>();

}
