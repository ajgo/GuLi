package edu.gzhu.guli.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Banner extends BaseEntity<Long> {

  /*
   * <a>标签的title属性
   */
  @Column(nullable = false)
  private String title;
  
  /*
   * <img>标签的src属性
   */
  @Column(nullable = false, columnDefinition = "text")
  private String imageUrl;
  
  /*
  * <a>标签的hrel属性 
  */
  @Column(nullable = false, columnDefinition = "text")
  private String linkUrl;

  /*
   * 排序规则
   */
  private int sort = 0;
  
}
