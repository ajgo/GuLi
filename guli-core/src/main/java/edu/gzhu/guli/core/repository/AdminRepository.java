package edu.gzhu.guli.core.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import edu.gzhu.guli.core.entity.Admin;
import edu.gzhu.guli.core.repository.support.BaseRepository;

@Repository
public interface AdminRepository extends BaseRepository<Admin, Long> {
  Optional<Admin> findByUsername(String username);
}
