package edu.gzhu.guli.core.repository;

import org.springframework.stereotype.Repository;

import edu.gzhu.guli.core.entity.CourseOrder;
import edu.gzhu.guli.core.repository.support.BaseRepository;

import java.util.List;

@Repository
public interface CourseOrderRepository extends BaseRepository<CourseOrder, Long> {
    List<CourseOrder> findCourseOrderByMemberId(Long memberId);
}
