package edu.gzhu.guli.core.video;

import java.io.InputStream;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoInfoRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoInfoResponse;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class VideoService {
  public String upload(MultipartFile file) {
    try {
      InputStream inputStream = file.getInputStream();
      // title 上传之后显示的名称
      String filename = file.getOriginalFilename();// 得到上传的文件名
      String title = filename.substring(0, filename.lastIndexOf("."));
      UploadStreamRequest request = new UploadStreamRequest(VideoConfiguration.ACCESS_KEY_ID,
          VideoConfiguration.ACCESS_KEY_SECRET, title, filename, inputStream);
      request.setApiRegionId(VideoConfiguration.API_REGION_ID);
      UploadVideoImpl uploader = new UploadVideoImpl();
      UploadStreamResponse response = uploader.uploadStream(request);
      log.info("RequestId = {}", response.getRequestId());

      String videoSourceId = null;
      if (response.isSuccess()) {
        videoSourceId = response.getVideoId();
        log.info("VideoSourceId = {}", videoSourceId);
      } else {
        videoSourceId = response.getVideoId();
        log.info("VideoSourceId = {}", videoSourceId);
        log.info("ErrorCode = {}", response.getCode());
        log.info("ErrorMessage = {}", response.getMessage());
      }

      return videoSourceId;
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      return null;
    }
  }

  public boolean validSourceId(String sourceId) {
    DefaultAcsClient client = VideoConfiguration.initVodClient();
    GetVideoInfoResponse response = new GetVideoInfoResponse();
    try {
      log.info("start check source id : {}", sourceId);
      GetVideoInfoRequest request = new GetVideoInfoRequest();
      request.setVideoId(sourceId);
      response = client.getAcsResponse(request);
      log.info("source id : {} pass check. find video title is : {}", sourceId, response.getVideo().getTitle());
    } catch (Exception e) {
      log.error("check source id : {} failure, error message : {}", sourceId, e.getLocalizedMessage());
      return false;
    }
    return true;
  }

  public boolean delete(String videoSourceId) {
    try {
      DefaultAcsClient client = VideoConfiguration.initVodClient();
      DeleteVideoRequest request = new DeleteVideoRequest();
      request.setVideoIds(videoSourceId);
      client.getAcsResponse(request);
      return true;
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      e.printStackTrace();
    }
    return false;
  }

  public String getVideoAuth(String sourceId) {
    try {
      // 1.1创建初始化对象
      // 创建初始化对象
      DefaultAcsClient client = VideoConfiguration.initVodClient();
      // 1.2创建获取视频地址response和request对象
      GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
      GetVideoPlayAuthResponse response = new GetVideoPlayAuthResponse();
      // 1.3向request对象里面设置视频id
      request.setVideoId(sourceId);
      // 1.4调用初始化对象里的方法，取得凭证
      response = client.getAcsResponse(request);
      String playAuth = response.getPlayAuth();
      return playAuth;
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }
    return null;
  }
}
