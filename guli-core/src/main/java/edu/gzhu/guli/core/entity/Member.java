package edu.gzhu.guli.core.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Member extends BaseEntity<Long> {

  /*
   * 用户名
   */
  @Column(nullable = false, unique = true)
  private String username;
  
  /*
  * 密码
  */
  @Column(nullable = false)
  private String password;
  
  /*
  * 姓名
  */
  @Column(nullable = false)
  private String name;

  /*
   * 手机号
   */
  @Column(nullable = true, unique = true)
  private String phone;

  /*
   * 用户微信open_id
   */
  private String openId;

  /*
   * 用户头像url
   */
  @Column(nullable = false, columnDefinition = "text")
  private String avatar;

  /*
   * 用户的角色，均为MEMBER
   */
  @ManyToOne
  @JoinColumn(name = "role_id", nullable = false)
  private Role role;

  /*
   * 用户参加的课程
   */
  @OneToMany(mappedBy = "member", cascade = { CascadeType.ALL })
  private List<MemberJoinCourse> courses = new ArrayList<>();
}
