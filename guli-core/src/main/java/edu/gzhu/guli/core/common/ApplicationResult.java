package edu.gzhu.guli.core.common;

import java.util.Arrays;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationResult<T> {
  public static final int SUCCESE = 1;
  public static final int FAILURE = 0;

  private int status;

  private T data;

  private String[] message;

  public ApplicationResult<T> message(String... msgs) {
    this.message = Arrays.copyOf(msgs, msgs.length);
    return this;
  }

  public static <T> ApplicationResult<T> succese() {
    ApplicationResult<T> result = new ApplicationResult<>();
    result.setData(null);
    result.setStatus(SUCCESE);
    return result;
  }
  
  public static <T> ApplicationResult<T> succese(T data) {
    ApplicationResult<T> result = new ApplicationResult<>();
    result.setData(data);
    result.setStatus(SUCCESE);
    return result;
  }

  public static <T> ApplicationResult<T> failure(String... message) {
    ApplicationResult<T> result = new ApplicationResult<>();
    result.message(message);
    result.setStatus(FAILURE);
    return result;
  }
}
