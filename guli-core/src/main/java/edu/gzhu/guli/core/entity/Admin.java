package edu.gzhu.guli.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Admin extends BaseEntity<Long> {

  /*
   * 用户名
   */
  @Column(nullable = false, unique = true)
  private String username;

  /*
   * 密码
   */
  @Column(nullable = false)
  private String password;

  /*
   * 姓名
   */
  @Column(nullable = false)
  private String name;

  @ManyToOne
  @JoinColumn(name = "role_id", nullable = false)
  private Role role;

}
