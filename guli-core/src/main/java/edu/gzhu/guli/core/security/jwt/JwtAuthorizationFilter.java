package edu.gzhu.guli.core.security.jwt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationConverter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final JwtAuthenticationConverter authenticationConverter = new JwtAuthenticationConverter();

  public JwtAuthorizationFilter(AuthenticationManager authenticationManager) {
    super(authenticationManager);
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request,
      HttpServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    UsernamePasswordAuthenticationToken authRequest = authenticationConverter.convert(request);
    if (authRequest == null) {
      chain.doFilter(request, response);
      return;
    }

    String username = authRequest.getName();
    if (authenticationIsRequired(username)) {
      SecurityContextHolder.getContext().setAuthentication(authRequest);
    }
    chain.doFilter(request, response);
  }

  private boolean authenticationIsRequired(String username) {
    Authentication existingAuth = SecurityContextHolder.getContext().getAuthentication();
    if (existingAuth != null && existingAuth.isAuthenticated()) {
      if (existingAuth instanceof UsernamePasswordAuthenticationToken && !existingAuth.getName().equals(username)) {
        return true;
      } else {
        return existingAuth instanceof AnonymousAuthenticationToken;
      }
    } else {
      return true;
    }
  }

  private class JwtAuthenticationConverter implements AuthenticationConverter {

    public static final String AUTHENTICATION_SCHEME_JWT = SecurityConstants.TOKEN_PREFIX;

    @Override
    public UsernamePasswordAuthenticationToken convert(HttpServletRequest request) {
      String header = request.getHeader(SecurityConstants.TOKEN_HEADER);
      if (header == null) {
        return null;
      }
      header = header.trim();
      if (!StringUtils.startsWithIgnoreCase(header, AUTHENTICATION_SCHEME_JWT)) {
        return null;
      }

      String token = header.substring(7);
      try {
        if (JwtTokenUtils.isTokenExpired(token)) return null;
        String username = JwtTokenUtils.getUsernameByToken(token);
        logger.info("checking username: {}", username);
        List<SimpleGrantedAuthority> userRoles = JwtTokenUtils.getUserRolesByToken(token);
        if (!StringUtils.hasText(username)) {
          return new UsernamePasswordAuthenticationToken(username, null, userRoles);
        }
      } catch (Exception e) {
        logger.warn("Request to parse JWT with invalid signature . Detail : {}", e.getMessage());
      }
      return null;
    }
  }
}
