package edu.gzhu.guli.core.repository;

import org.springframework.stereotype.Repository;

import edu.gzhu.guli.core.entity.Banner;
import edu.gzhu.guli.core.repository.support.BaseRepository;

import java.util.List;


@Repository
public interface BannerRepository extends BaseRepository<Banner, Long> {
    List<Banner> findByOrderBySortAsc();
}
